namespace :digest do

  namespace :new_quiz do
    task send_daily: :environment do
      Digester.new(:new_quiz, :daily).run
    end

    task set_defaults: :environment do
      Digester.set_new_quiz_defaults!
    end
  end

  namespace :graded_quiz do
    task send_daily: :environment do
      Digester.new(:graded_quiz, :daily).run
    end

    task set_defaults: :environment do
      Digester.set_graded_quiz_defaults!
    end
  end

  task remove_false: :environment do
    Digester.remove_false_from_notifications!
  end

end
