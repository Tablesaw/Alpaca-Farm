$(document).on ("page:change", function() {
  enforceMinMax();
  // Reveal answer on click.
  $( ".click-to-reveal-answer" ).click(function() {
    var clicked = $( this );
    var number = clicked.attr("question-number");
    var concealed = $( ".concealed-answer[question-number=" + number + "]" );
    clicked.hide();
    concealed.show();
  });

  // $( ".move-forward ").click(function() {
  $( "#form-questions-row ").on('click', '.move-forward', function() {
    var id = $(this).data("id");
    var current_info = $('.quiz-form-question-column[data-id='+id+']');
    var next = current_info.nextAll(".form-question-clearfix");
    if (next.length > 1) {
      current_info.insertAfter(next[1]);
      current_info.after(next[0]);
      updateNumbers();
    }
  });

  // $( ".move-back ").click(function() {
  $( "#form-questions-row ").on('click', '.move-back', function() {
    var id = $(this).data("id");
    var current_info = $('.quiz-form-question-column[data-id='+id+']');
    var prev = current_info.prevAll(".form-question-clearfix");
    current_info.nextAll(".form-question-clearfix").first().remove();
    if (prev.length > 0) {
      prev.first().prevUntil(".form-question-clearfix").last().before(current_info);
      current_info.after('<p class="form-question-clearfix"></p>');
      updateNumbers();
    }
  });

  $(".add-new-question").click(function() {
    var columns = $('.quiz-form-question-column');
    var newColumn = columns.first().clone()
    var columnCount = columns.size();
    var inputs = newColumn.find('input, textarea');
    inputs.val('');
    inputs.attr('id', function (index, old) {
      return old.replace(/\d+/, columnCount)
    })
    inputs.attr('name', function (index, old) {
      return old.replace(/\d+/, columnCount)
    })
    newColumn.find('label').attr('for', function (index, old) {
      return old.replace(/\d+/, columnCount);
    })
    newColumn.attr('data-id', columnCount);
    newColumn.find('button').attr('data-id', columnCount);
    newColumn.insertAfter( $('.form-question-clearfix').last() );
    newColumn.after('<p class="form-question-clearfix"></p>');
    newColumn.show();
    updateNumbers();
  })

  // $(".delete-question").click(function() {
  $( "#form-questions-row ").on('click', '.delete-question', function() {
    var id = $(this).data("id");
    var current_info = $('.quiz-form-question-column[data-id='+id+']');
    current_info.find('#quiz_questions_attributes_'+id+'__destroy').val('true');
    current_info.hide();
    current_info.find('.movable-question-number-value').each(function () {
      $(this).val("");
    });

    current_info.nextAll(".form-question-clearfix").first().remove();
    updateNumbers();
  })
})

function updateNumbers() {
  $('.quiz-form-question-column:visible').each(function (idx) {
    $(this).find('.movable-question-number-text').each(function () {
      $(this).html(idx + 1);
    });
    $(this).find('.movable-question-number-value').each(function () {
      $(this).val(idx + 1);
    });
  });
  enforceMinMax();
}

function enforceMinMax() {
  var questionCount = $('.quiz-form-question-column:visible').size();
  if (questionCount > 5) {
    $('.delete-question').attr("disabled", null);
  } else {
    $('.delete-question').attr("disabled", true);
  }
  if (questionCount < 25) {
    $('.add-new-question').attr("disabled", null);
  } else {
    $('.add-new-question').attr("disabled", true);
  }
}
