$(document).on ("page:change", function() {

  $("#mark-remaining-incorrect").click(function(){
    $(".response-row").each(function(){
      var radios = $(this).find("[type=radio]");
      var checked = false;
      radios.each(function(){
        if (this.checked) {
          checked = true;
          return false;
        }
      });
      if (!checked) {
        radios.each(function(){
          if ($(this).val() === "false") {
            this.checked = true
            return false
          }
        });
      }
    });
  });
});
