module ApplicationHelper

  # l raises an error when it tries to localize nil.
  # Localize_agdt returns TBD if agdt (Approximate_grading_date_time) is nil,
  # and localizes otherwise
  def localize_agdt (agdt, format)
    if agdt
      l agdt, format: format
    else
      "TBD"
    end
  end

  def quiz_status(quiz)
    if not(quiz.opened)
      "unready"
    elsif quiz.playable?
      "playable"
    elsif quiz.being_graded?
      "being graded"
    else
      "completed"
    end
  end

  def result_color(response)
    if response.correct && response.money
      "success"
    elsif response.correct
      "warning"
    elsif response.money
      "danger"
    end
  end

  def url_with_protocol(url)
    /^http/i.match(url) ? url : "http://#{url}"
  end

  def play_button(quiz)
    if @current_player
      if @current_player.participated_quizzes.pluck(:id).include? quiz.id
        raw("<span class='glyphicon glyphicon-ok' aria-hidden='true'></span>")
      elsif quiz.author == @current_player
        raw("<span class='glyphicon glyphicon-star' aria-hidden='true'></span>")
      else
        if quiz.playable?
          link_to "Play", quiz, class: "btn btn-default btn-sm"
        end
      end
    end
  end

  def index_time(quiz)
    if quiz.playable?
      quiz.approximate_grading_date_time
    else
      quiz.closing_date_time
    end
  end

  def current_question_text(question)
    if question.question_edited_at
      question.edited_text +
      " <i>(edited at #{l question.question_edited_at, format: :basic})</i>"
    else
      question.question_text
    end
  end

  def current_answer(question)
    if question.answer_edited_at
      question.edited_answer +
      " (edited at #{l question.answer_edited_at, format: :basic})"
    else
      question.answer
    end
  end

end
