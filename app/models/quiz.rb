class Quiz < ActiveRecord::Base

  # ActiveRecord Associations
  has_many :questions
  accepts_nested_attributes_for :questions,
    allow_destroy: true

  has_many :response_sheets
  has_many :responses, through: :response_sheets
  belongs_to :author, class_name: "Player"
  has_many :participants, source: :player,
                          through: :response_sheets,
                          foreign_key: "player_id"
  has_many :champions, -> { where(rank: 1).includes(:player) },
                       class_name: "ResponseSheet"

  # Default validations
  validates :author_id, :subject, :base_score, :money_question_count,
            presence: true
  validates :base_score, :money_question_count,
            numericality: {
              only_integer: true,
              greater_than: 0
              }
  validate :validate_question_count

  # Custom validations
  validates_with FinalizationValidator, on: :finalize

  scope :unready,       -> {where(opened: nil)}
  scope :active,        -> {where(opened: true)}
  scope :playable,      -> {active.where(closed: nil)}
  scope :being_graded,  -> {active.where(closed: true).where(complete: nil)}
  scope :complete,      -> {where(complete: true)}
  scope :title_search,  ->(query) {active.where("subject ilike ?", "%#{query}%")}

  # Return true if a quiz is active (opened, but not closed)
  def playable?
    self.opened && not(self.closed)
  end

  def being_graded?
    self.closed && not(self.complete)
  end

  # Set default values for quizzes when getting a new quiz.
  def set_defaults
    self.base_score = 15
    self.money_question_count = 5
    for i in 1..12
      self.questions.build(number: i)
    end
  end

  # Set record values when a quiz goes live
  def start_quiz(approximate_grading_date_time)
    self.update(approximate_grading_date_time: approximate_grading_date_time,
                opening_date_time: Time.now,
                opened: true)
    if self.author.opened_quizzes_count
      self.author.opened_quizzes_count += 1
    else
      self.author.opened_quizzes_count = 1
    end

    self.author.save
  end

  def close_quiz!
    self.closed = true
    self.closing_date_time = Time.now
    self.save
  end

  def percent_graded
    total_graded = 0.0
    self.questions.each { |q| total_graded += q.percent_graded}
    total_graded /= self.questions.size
  end

  def complete_quiz
    self.calculate_results
    self.update(complete: true)
  end

  def calculate_results
    self.questions.each { |q| q.set_money_value! }

    self.set_maximum_score!

    self.response_sheets.each { |rs| rs.set_score! }

    self.set_ranks!

    self.response_sheets.reload

    self.response_sheets.each { |rs| rs.set_percentile_rank! }

    self.response_sheets.each { |rs| rs.set_percent_of_champion! }
  end

  def set_maximum_score!
    maximum_score = self.questions.
                         order(money_value: :asc).
                         last(self.money_question_count).
                         sum{|q| q.money_value}
    maximum_score += (self.questions_count * self.base_score)
    self.update(maximum_score: maximum_score)
  end

  def set_ranks!
    self.response_sheets.order(score: :desc).each_with_index do |rs, i|
      rs.update(rank: i+1)
    end
    self.response_sheets.order(rank: :asc).each_cons(2) do |first, second|
      second.update(rank: first.rank) if first.score == second.score
    end
  end

  private

  def validate_question_count
    remaining_questions = self.questions.reject(&:marked_for_destruction?).length
    errors.add :questions, "You must have at least five questions" if remaining_questions < 5
    errors.add :questions, "You may have no more than twenty-five questions" if remaining_questions > 25
    if self.money_question_count && self.money_question_count > remaining_questions
      self.money_question_count = (remaining_questions * 0.4).round
    end
  end

end
