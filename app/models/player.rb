class Player < ActiveRecord::Base
  store_accessor :notifications, :new_quiz
  store_accessor :notifications, :graded_quiz


  #ActiveRecord associations
  belongs_to :user
  has_many :authored_quizzes, class_name: "Quiz",
                              foreign_key: "author_id"
  has_many :participated_quizzes, class_name: "Quiz",
                                  source: :quiz,
                                  through: :response_sheets,
                                  foreign_key: "quiz_id"
  has_many :response_sheets
  has_many :notices

  # Default validations
  validates :name, presence: true, uniqueness: {case_sensitive: false},
            length: {in: 3..20},
            format: {with: /\A[a-zA-Z0-9-]+\Z/,
                     message: "only allows letters, numbers, and hyphens (-)"}

end
