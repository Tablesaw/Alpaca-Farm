class Response < ActiveRecord::Base
  belongs_to :question, counter_cache: true, touch: true
  belongs_to :response_sheet
  has_one :player, through: :response_sheet
  has_one :quiz, through: :response_sheet

  validates :money, exclusion: { in: [nil] }

  scope :graded, ->  {where.not(correct: nil)}
  scope :correct, -> {where(correct: true)}
end
