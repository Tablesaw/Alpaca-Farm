class Notice::NewQuiz < Notice
  belongs_to :about, class_name: "Quiz"
  alias quiz about

  validates_uniqueness_of :recipient_id, scope: :about_id

  def async_mailer
    NoticeMailer.new_quiz(self).deliver_later
  end

  def sendable?
    quiz.playable? &&
      Time.now.utc < quiz.approximate_grading_date_time &&
      self.recipient.user.confirmed?
  end

end
