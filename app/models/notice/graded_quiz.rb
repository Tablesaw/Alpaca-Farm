class Notice::GradedQuiz < Notice
  belongs_to :about, class_name: "ResponseSheet"
  alias response_sheet about

  validates_uniqueness_of :about_id

  def async_mailer
    NoticeMailer.graded_quiz(self).deliver_later
  end

  def sendable?
    response_sheet.graded? && self.recipient.user.confirmed?
  end

end
