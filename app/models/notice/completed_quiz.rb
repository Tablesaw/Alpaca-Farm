class Notice::CompletedQuiz < Notice
  belongs_to :about, class_name: "Quiz"
  alias quiz about

  validates_uniqueness_of :recipient_id, scope: :about_id

  def send_notice
    nil
  end

  def sendable?
    self.quiz.complete && self.recipient.user.confirmed?
  end

end
