class Notice < ActiveRecord::Base
  class SendableValidator < ActiveModel::Validator
    def validate(record)
      unless record.sendable?
        record.errors[:base] << "Notice cannot be sent at present"
      end
    end
  end

  validates_with SendableValidator
  belongs_to :recipient, class_name: "Player"
  validates_presence_of :about, :recipient

  scope :unsent, -> { where(sent_at: nil) }
  scope :unsent_since, -> (start_time) { where("created_at >= ?", start_time).
                                           unsent }

  def send_notice
    if should_send?
      async_mailer
      mark_sent
    end
  end

  # Subclass should overwrite methods below
  def sendable?
    self.recipient.user.confirmed?     # placeholder, determine best default
  end

  def should_send?
    !sent_at  # placeholder, overwrite when appropriate
  end

  private

  def mark_sent
    self.update(sent_at: Time.now.utc)
  end

end
