class Question < ActiveRecord::Base
  belongs_to :quiz, counter_cache: true
  has_many :responses, autosave: true

  validates :number, numericality: { only_integer: true }
  validates :question_text, :answer, presence: true, on: :finalize

  BLANK_STRING = "*~*~* BLANK STRING *~*~*"

  def responses_graded_count
    Rails.cache.fetch [self, "responses_graded_count"] do
      responses.graded.size
    end
  end

  def responses_moneyed_count
    Rails.cache.fetch [self, "responses_moneyed_count"] do
      responses.where(money: true).size
    end
  end

  def responses_correct_count
    Rails.cache.fetch [self, "responses_correct_count"] do
      responses.correct.size
    end
  end

  def percent_graded
    if self.responses_count != 0
      ( self.responses_graded_count.to_f / self.responses_count.to_f ) * 100
    else
      0.0
    end
  end

  def percent_moneyed
    if self.responses_count != 0
      ( self.responses_moneyed_count.to_f / self.responses_count.to_f ) * 100
    end
  end

  def percent_correct
    if self.responses_graded_count != 0
      ( self.responses_correct_count.to_f / self.responses_graded_count.to_f ) * 100
    end
  end

  def set_money_value!
    self.update(money_value: [100 - self.percent_correct.floor, 99].min)
  end

  def update_question(new_text)
    unless self.edited_text || new_text.blank? || new_text == self.question_text
      self.edited_text = new_text
      self.question_edited_at = Time.now
      self.save
    end
  end

  def update_answer(new_text)
    unless self.edited_answer || new_text.blank? || new_text == self.answer
      self.edited_answer = new_text
      self.answer_edited_at = Time.now
      self.save
    end
  end

  def responses_hash
    resp_hash = {}
    self.responses.each do |response|
      processed_response = Question.prepare_string(response.response_text)

      if resp_hash.has_key? processed_response
        resp_hash[processed_response][:count] += 1
      else
        resp_hash[processed_response] =
          {correct: response.correct, count: 1}
      end
    end
    resp_hash
  end

  def self.prepare_string(raw_string)
    if raw_string.blank?
      BLANK_STRING
    else
      raw_string.
        tr("[","(").
        tr("]",")").
        upcase
    end
  end

end
