class GradingForm
  include ActiveModel::Model

  attr_accessor :responses, :question

  def initialize (attributes_hash = {})
    if attributes_hash.has_key?(:question)
      @question = attributes_hash[:question]
    elsif attributes_hash.has_key?(:question_id)
      @question = Question.find(attributes_hash[:question_id])
    end

    if attributes_hash.has_key?(:responses)
      @responses = attributes_hash[:responses]
    else
      @responses = @question.responses_hash
      processed_answer = Question.prepare_string @question.answer
      if @responses.has_key? Question::BLANK_STRING
        @responses[Question::BLANK_STRING][:correct] = false
      end
      if @responses.has_key? processed_answer
        @responses[processed_answer][:correct] = true
      end
    end
  end

  def save
    self.question.responses.each do |response|
      if response.response_text.blank?
        processed_response = Question::BLANK_STRING
      else
        processed_response = response.response_text.tr("[","(").tr("]",")").upcase
      end
      if correct_hash = self.responses[processed_response]
        response.correct = correct_hash[:correct]
      end
    end

    self.question.save
  end

end
