class Digester
  def initialize(notice_setting, frequency_setting)
    @notice_type = if notice_whitelist.include?(notice_setting)
                     notice_setting.to_s
                   else
                     false
                   end
    @frequency = if frequency_whitelist.include?(frequency_setting)
                   frequency_setting.to_s
                 else
                   false
                 end
  end

  def run
    if @notice_type && @frequency
      Player.where("notifications @> hstore(:key, :value)",
             key: @notice_type, value: @frequency).find_each do |player|
        DigestWorker.perform_async(player.id, @notice_type)
      end
    end
  end

  # These methods are to be used to modify the database so that the player
  # notifications hstore has values for digests. These will need to be run using
  # the rake tasks in lib/digesting.rake

  def self.set_new_quiz_defaults!
    Player.where("notifications @> hstore(:key, :value)",
           key: "new_quiz", value: "true").find_each do |player|
      player.update(new_quiz: "daily")
    end
  end

  def self.set_graded_quiz_defaults!
    Player.where("notifications @> hstore(:key, :value)",
           key: "graded_quiz", value: "true").find_each do |player|
      player.update(graded_quiz: "immediately")
    end
  end

  def self.remove_false_from_notifications!
    Player.where("notifications @> hstore(:key, :value)",
           key: "graded_quiz", value: "false").find_each do |player|
      player.update(graded_quiz: nil)
    end

    Player.where("notifications @> hstore(:key, :value)",
           key: "new_quiz", value: "false").find_each do |player|
      player.update(new_quiz: nil)
    end
  end

  protected

  def frequency_whitelist
    [ :daily ]
  end

  def notice_whitelist
    [ :new_quiz, :graded_quiz ]
  end
end
