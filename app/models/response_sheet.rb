class ResponseSheet < ActiveRecord::Base
  belongs_to :player, counter_cache: true
  belongs_to :quiz, counter_cache: true
  has_many :responses

  accepts_nested_attributes_for :responses

  scope :champions, -> {where(rank: 1)}
  scope :runners_up, -> {where(rank: 2)}

  validates :quiz, presence: true
  validates :player, presence: true
  validate  :validate_responses

  def graded?
    !score.nil?
  end

  # Create responses associated with the correct questions
  def create_responses
    self.quiz.questions.each do |question|
      self.responses.build(question: question)
    end
  end

  def set_score!
    score = 0
    self.responses.each do |r|
      if r.correct
        score += self.quiz.base_score
        if r.money
          score += r.question.money_value
        end
      end
    end
    self.update(score: score)
  end

  def set_percentile_rank!
    scores = self.quiz.response_sheets_count
    less_than_current = self.quiz.
                             response_sheets.
                             where("rank > ?", self.rank).
                             count

    equal_to_current  = self.quiz.
                             response_sheets.
                             where(rank: self.rank).
                             count

    pr = 100 * (( less_than_current + (0.5 * equal_to_current)) / scores )
    self.update(percentile_rank: pr)
  end

  def set_percent_of_champion!
    poc = 100 *( self.score.to_f / self.quiz.response_sheets.maximum(:score) )
    self.update(percent_of_champion: poc)
  end

  private

  def validate_responses
    if self.quiz
      unless self.quiz.questions.size == self.responses.size
        self.errors[:base] << "Responses to this quiz need"\
                              "#{self.quiz.questions.count} responses"
      end
      moneys = 0
      self.responses.each {|r| moneys += 1 if r.money}
      unless self.quiz.money_question_count == moneys
        self.errors[:base] << "You did not select exactly "\
                              "#{self.quiz.money_question_count} responses "\
                              "to be moneyed"
      end
    end
  end


end
