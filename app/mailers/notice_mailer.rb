class NoticeMailer < ApplicationMailer
  default from: "notifications@alpacafarmtrivia.com"

  def new_quiz(notice)
    @quiz = notice.about
    @player = notice.recipient

    mail(to: "#{@player.name} <#{@player.user.email}>",
         subject: "New Quiz at Alpaca Farm: #{@quiz.subject}")
  end

  def graded_quiz(notice)
    @result = notice.about
    @player = notice.recipient
    @result.quiz.author
    @result.quiz.champions
    subject = @result.rank.ordinalize + " place at Alpaca Farm: " +
              @result.quiz.subject + "!"

    mail(to: "#{@player.name} <#{@player.user.email}>",
         subject: subject)
  end

  def new_quiz_digest(player, notices)
    @player = player
    @quizzes = Quiz.where(id: notices.pluck(:about_id))
                   .includes(:author)
                   .order(:opening_date_time)

    mail(to: "#{@player.name} <#{@player.user.email}>",
         subject: "New Quizzes at Alpaca Farm")
  end

  def graded_quiz_digest(player, notices)
    @player = player
    @results = ResponseSheet.where(id: notices.pluck(:about_id))
                            .includes(quiz: [:author, :champions])
    mail(to: "#{@player.name} <#{@player.user.email}>",
         subject: "Your Recent Results at Alpaca Farm")
  end

end
