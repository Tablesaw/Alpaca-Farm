class DigestWorker
  include Sidekiq::Worker

  def perform(player_id, notice_type)
    player = Player.find(player_id)

    send_notices(player, notice_type.to_s)
  end

  def send_notices(player, notice_type)
    start_time =  if player.send(notice_type.to_sym) == "daily"
                    Time.now.utc - (1.5).days
                  end
    if start_time
      case notice_type
      when "new_quiz"
        notices = Notice::NewQuiz.where(recipient_id: player.id).
                                  unsent_since(start_time)
      when "graded_quiz"
        notices = Notice::GradedQuiz.where(recipient_id: player.id).
                                     unsent_since(start_time)
      end

      if notices.size > 0
        NoticeMailer.send(notice_type + "_digest", player, notices).deliver_now
      end

      notices.update_all(sent_at: Time.now.utc)
    end
  end
end
