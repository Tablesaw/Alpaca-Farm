class CompletedQuizWorker
  include Sidekiq::Worker

  def perform(quiz_id)
    quiz = Quiz.find(quiz_id)
    create_notices(quiz)
  end

  def create_notices(quiz)
    quiz.response_sheets.each do |rs|
      if rs.player.graded_quiz
        n = Notice::CompletedQuiz.create(about: quiz, recipient: rs.player)
        n.send_notice if n.valid? && rs.player.graded_quiz == "immediately"
      end
    end
  end

end
