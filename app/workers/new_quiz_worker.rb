class NewQuizWorker
  include Sidekiq::Worker

  def perform(quiz_id)
    quiz = Quiz.find(quiz_id)
    create_notices(quiz)
  end

  def create_notices(quiz)
    Player.where("defined(notifications, ?)", "new_quiz").find_each do |player|
      n = Notice::NewQuiz.create(about: quiz, recipient: player)
      n.send_notice if n.valid? && player.new_quiz == "immediately"
    end
  end
end
