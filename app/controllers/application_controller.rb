class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :set_user_time_zone

  private

  def set_user_time_zone
    Time.zone = current_user.player.time_zone if user_signed_in?
  end
end
