class PlayersController < ApplicationController
  before_action :set_player, only: [:show, :edit, :update]
  before_action :authorize!, only: [:edit, :update]

  # GET /players
  def index
    @players = Player.paginate(page: params[:page], per_page: 50).
                      order(:name)
  end

  # GET /players/1
  def show
      @authored_quizzes = @player.authored_quizzes.includes(:champions).order(opening_date_time: :desc)
      @response_sheets = @player.response_sheets.includes(quiz: :author).
                                 paginate(page: params[:page], per_page: 50).
                                 order(created_at: :desc)
  end

  # GET /players/1/edit
  def edit
    nil_to_string(:new_quiz, :graded_quiz)
  end

  # PATCH/PUT /players/1
  def update
    params_to_nil("new_quiz", "graded_quiz")

    if @player.update(player_params)
      redirect_to @player, notice: 'Player was successfully updated.'
    else
      render :edit
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_player
    @player = Player.find(params[:id])
    @can_update = user_signed_in? && current_user.player == @player
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def player_params
    params.require(:player).permit(:location, :motto, :time_zone, :new_quiz, :graded_quiz)
  end

  # If a user is not authorized, redirect to showing the player template
  def authorize!
    if not(@can_update)
      redirect_to player_path(@player),
        alert: "You are not authorized to edit this profile."
    end
  end

  # Change the string "nil" to nil value in params that will be saved in hstore
  def params_to_nil (*whitelist_keys)
    hstore_hash = params["player"].select do |key, value|
      whitelist_keys.include? key
    end
    if hstore_hash.has_value? "nil"
      hstore_hash.each do |key, value|
        params["player"][key] = nil if value == "nil"
      end
    end
  end

  # Change nil values in notifications to the sting "nil" for handling in HTML
  def nil_to_string (*attributes)
    attributes.each do |attribute|
      if @player.send(attribute) == nil
        @player.send(attribute.to_s + "=", "nil")
      end
    end
  end
end
