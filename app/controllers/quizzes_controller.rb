class QuizzesController < ApplicationController
  before_action :set_quiz, only: [:show, :edit, :update, :destroy, :finalize,
                                  :start_quiz]
  before_action :authenticate_user!, only: [:new, :create]
  before_action :authorize!, only: [:edit, :update, :destroy, :finalize,
                                    :start_quiz]
  before_action :check_unready!, only: [:finalize, :start_quiz]

  # GET /quizzes
  def index

    if params[:search]
      @quizzes = Quiz.title_search(params[:search]).
                      includes(:author, :champions).
                      paginate(page: params[:page], per_page: 50).
                      order(opening_date_time: :desc)
    else
      @quizzes = Quiz.active.includes(:author, :champions).
                      paginate(page: params[:page], per_page: 50).
                      order(opening_date_time: :desc)
    end

    @current_player = current_user.player if user_signed_in?
  end

  def home_page
    @current_player = current_user.player if user_signed_in?

    @playable_quizzes = Quiz.
      playable.
      includes(:author).
      where("approximate_grading_date_time > ?", 1.week.ago ).
      order(approximate_grading_date_time: :asc)
    @quizzes_being_graded = Quiz.being_graded.
                                 includes(:author).
                                 order(opening_date_time: :asc)
    if Quiz.complete.where('closing_date_time > ?', 1.week.ago).size > 10

      @complete_quizzes = Quiz.complete.
                               includes(:author, :response_sheets, :champions).
                               where('closing_date_time > ?', 1.week.ago).
                               order(closing_date_time: :desc)
    else
      @complete_quizzes = Quiz.complete.
                               includes(:author, :response_sheets, :champions).
                               order(closing_date_time: :desc).
                               limit(10)
    end
  end

  # GET /quizzes/1
  def show
    @response_sheet = nil
    @show_results = nil
    if not(@quiz.opened)
      authenticate_user!
      authorize!
    elsif (@quiz.complete)
      @show_results = true
      if user_signed_in?
        @response_sheet = @quiz.response_sheets.
                                includes(:player, responses: [:question]).
                                find_by player: current_user.player
      end
    elsif(@quiz.playable?)
      if not(@can_edit)
        # authenticate_user!
        if user_signed_in?
          @response_sheet = @quiz.response_sheets.find_or_initialize_by(
            player: current_user.player
            )
          @disabled = false
        else
          @response_sheet = @quiz.response_sheets.new
          @disabled = true
        end
        if @response_sheet.new_record?
          @response_sheet.create_responses
        end
      end
    elsif @quiz.being_graded?
      if @can_edit
        redirect_to grading_path(@quiz)
      else
        if user_signed_in?
          @response_sheet = @quiz.response_sheets.
                                  includes(:player, responses: [:question]).
                                  find_by(player: current_user.player)
        end
      end
    end
  end

  # GET /quizzes/new
  def new
    @quiz = Quiz.new
    @quiz.set_defaults
  end

  # GET /quizzes/1/edit
  def edit

  end

  # POST /quizzes
  def create
    @quiz = current_user.player.authored_quizzes.new(quiz_params)
    if @quiz.save
      redirect_to @quiz, notice: 'Quiz was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /quizzes/1
  def update
    if @quiz.update(quiz_params)
      redirect_to @quiz, notice: 'Quiz was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /quizzes/1
  def destroy
    @quiz.destroy
    redirect_to quizzes_path, notice: 'Quiz was successfully destroyed.'
  end

  # GET finalize/1
  def finalize
    if @quiz.invalid?(:finalize)
      render 'edit'
    end
  end

  # POST start_quiz/1
  def start_quiz
    if @quiz.invalid?(:finalize)
      return render 'edit'
    end

    agdt = params[:quiz][:approximate_grading_date_time]

    if agdt < 8.hours.from_now
      return redirect_to finalize_path(@quiz),
          alert: "Quizzes must be open for at least eight hours."
    end


    if @quiz.start_quiz(agdt)
      # NoticeMailer.new_quiz(@quiz).deliver_later
      NewQuizWorker.perform_async(@quiz.id)
      redirect_to root_path, notice: "Your quiz is now live!"
    else
      render 'finalize', alert: "There was a problem starting your quiz."
    end

  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_quiz
    @quiz = Quiz.includes(:author, :questions).order('questions.number').find(params[:id])
    @can_edit = user_signed_in? && current_user.player == @quiz.author
  end

  # Never trust parameters from the scary internet, only allow the white list
  # through.
  def quiz_params
    params.require(:quiz).permit(:subject, :base_score,
                                 :money_question_count, :description,
                                 questions_attributes: [:id, :number,
                                                        :question_text,
                                                        :external_link,
                                                        :answer,
                                                        :grading_notes,
                                                        :_destroy])
  end

  # If a user is not authorized, redirect to the root
  def authorize!
    if not(@can_edit)
      redirect_to root_path,
                alert: "You are not authorized to view or edit this quiz."
    end
  end

  # If a quiz is already opened, redirect to quiz.
  def check_unready!
    if @quiz.opened
      redirect_to @quiz, alert: "This quiz has already been started."
    end
  end


end
