class GradingController < ApplicationController
  before_action :authenticate_user!
  before_action :set_quiz
  before_action :set_question, only: :question
  before_action :authorize!

  # POST grading/start/1
  def start
    if @quiz.close_quiz!
      redirect_to grading_path(@quiz), notice: "This quiz is closed for grading"
    else
      redirect_to @quiz, alert: "This quiz could not be closed"
    end
  end

  # GET grading/1
  def show
  end

  # GET /grading/1/1
  def question
    @grading_form = GradingForm.new(question: @question)
  end

  # POST /grading/1/1
  def save_grading
    grading_form = GradingForm.new(grading_params)
    grading_form.save
    redirect_to grading_path(grading_form.question.quiz)
  end

  # POST grading/complete/1
  def complete
    if @quiz.percent_graded < 100
      redirect_to grading_path(@quiz),
                  alert: "This quiz still needs further grading"
    else
      if @quiz.complete_quiz
        GradedQuizWorker.perform_async(@quiz.id)
        # NoticeMailer.graded_quiz(@quiz).deliver_later
        redirect_to @quiz, notice: "This quiz is complete!"
      else
        redirect_to @quiz, alert: "There was an error completing this quiz."
      end
    end

  end

  private

  def set_quiz
    @quiz = Quiz.includes(questions: :responses).find(params[:quiz_id])
  end

  def set_question
    @question = @quiz.questions.find_by_number(params[:question_number])
  end

  def authorize!
    @can_grade = current_user.player == @quiz.author
    if not(@can_grade)
      redirect_to @quiz, alert: "This quiz is closed for grading."
    end
  end

  private

  def grading_params
    params.require(:grading_form).permit(:question_id).merge({
      responses: params.require(:grading_form).require(:responses).permit!
      })
  end

end
