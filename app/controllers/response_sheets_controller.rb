class ResponseSheetsController < ApplicationController
  before_action :authenticate_user!,   only: :create
  before_action :set_quiz!,            only: :create
  before_action :authorize_responder!, only: :create


  # POST /response_sheets
  def create
    @response_sheet = current_user.player.response_sheets.
                                   new(response_sheet_params)
    if @response_sheet.save
      redirect_to @response_sheet.quiz, notice: 'Successfully submitted!'
    else
      render template: "quizzes/show", notice: 'Your answers could not be submitted'
    end

  end

  private

    # Never trust parameters from the scary internet, only allow the white list
  # through.
  def response_sheet_params
    params.require(:response_sheet).
      permit(:quiz_id, responses_attributes: [:id, :question_id, :response_text,
        :money])
  end

  def set_quiz!
    @quiz = Quiz.find(params[:response_sheet][:quiz_id])
    unless @quiz.playable?
      redirect_to @quiz, alert: "This quiz is not open for participation."
    end
  end


  def authorize_responder!
    if @quiz.author == current_user.player
      redirect_to @quiz, alert: "You cannot play your own quiz!"
    end
    if @quiz.response_sheets.find_by_player_id(current_user.player)
      redirect_to @quiz,
        alert: "You have already submitted your answers for this quiz!"
    end
  end



end
