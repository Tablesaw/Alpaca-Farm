class QuestionsController < ApplicationController

  before_action :set_question!
  before_action :authenticate_user!, except: [:show]
  before_action :authorize_author!, except: [:show]
  before_action :only_use_active_quizzes!, except: [:show]

  def show
    if user_signed_in? && @quiz.author == current_user.player
      @can_edit = true
      @responses_hash = @question.responses_hash.sort
    end
    unless @quiz.complete? || @can_edit

      redirect_to quiz_path(@question.quiz),
        alert: "You cannot view that question summary right now"

    end
  end

  def edit_question
    if @question.question_edited_at
      redirect_to question_path(@quiz),
        alert: "You have already edited this question"
    end

  end

  def edit_answer
    if @question.answer_edited_at
      redirect_to question_path(@quiz),
        alert: "You have already edited this answer"
    end
  end

  def update
    if params[:edit_type] == "question"
      if @question.update_question(params[:question][:new_text])
        redirect_to question_path(@question)
      else
        render :edit_question
      end
    elsif params[:edit_type] == "answer"
      if @question.update_answer(params[:question][:new_answer])
        redirect_to question_path(@question)
      else
        render :edit_answer
      end
    else
      redirect_to quiz_path(@quiz), alert: "Invalid information"
    end
  end

  private

  def set_question!
    @question = Question.find(params[:id])
    @quiz = @question.quiz
  end

  def authorize_author!
    unless @quiz.author == current_user.player
      redirect_to quiz_path(@quiz),
        alert: "You cannot edit this answer"
    end
  end

  def only_use_active_quizzes!
    unless @quiz.opened
      redirect_to quiz_path(@quiz),
        alert: "You cannot view or edit individual questions unless the quiz is already opened"
    end
  end

end
