class FinalizationValidator < ActiveModel::Validator
  def validate(record)
    if record.questions.size == 0
      record.errors[:base] << "Quiz must have at least one question."
    else
      record.questions.each do |q|
        if q.invalid?(:finalize)
          record.errors[:base] << "Question #{q.number} is missing information."
        end
      end
    end
  end
end
