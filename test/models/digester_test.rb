require "test_helper"

class DigesterTest < ActiveSupport::TestCase

  class DigesterTest::Run < DigesterTest
    def setup
      FactoryBot.create_list(:player, 2, new_quiz: "daily")
      FactoryBot.create(:player, graded_quiz: "daily")
      FactoryBot.create(:player, new_quiz: "immediately")
      FactoryBot.create(:player)
    end

    test "run does not create a digest job when notice_setting is invalid" do
      assert_no_difference("DigestWorker.jobs.size") do
        Digester.new(:calculated_quiz, :daily).run
      end
    end

    test "run does not create a digest job when frequency_setting is invalid" do
      assert_no_difference("DigestWorker.jobs.size") do
        Digester.new(:new_quiz, :immediately).run
      end
    end

    test "run creates a digest job when notice_setting and frequency_setting "\
         "are valid" do
      assert_difference("DigestWorker.jobs.size", 2) do
        Digester.new(:new_quiz, :daily).run
      end
    end
  end

  class DigesterTest::RakeTasks < DigesterTest
    test "set_new_quiz_defaults! changes 'true' values of new_quiz "\
         "to 'daily'" do
      players = [
        FactoryBot.create(:player, new_quiz: "true"),
        FactoryBot.create(:player, new_quiz: "false"),
        FactoryBot.create(:player, new_quiz: nil),
        FactoryBot.create(:player)
        ]

      Digester.set_new_quiz_defaults!

      assert_equal "daily", players[0].reload.new_quiz
      assert_equal "false", players[1].reload.new_quiz
      assert_nil players[2].reload.new_quiz
      assert_nil players[3].reload.new_quiz
    end

    test "set_graded_quiz_defaults! changes 'true' values of graded_quiz "\
         "to 'immediately'" do
      players = [
        FactoryBot.create(:player, graded_quiz: "true"),
        FactoryBot.create(:player, graded_quiz: "false"),
        FactoryBot.create(:player, graded_quiz: nil),
        FactoryBot.create(:player)
        ]

      Digester.set_graded_quiz_defaults!

      assert_equal "immediately", players[0].reload.graded_quiz
      assert_equal "false", players[1].reload.graded_quiz
      assert_nil players[2].reload.graded_quiz
      assert_nil players[3].reload.graded_quiz
    end

    test "set_graded_quiz_defaults! changes 'false' values in notifications "\
         "to nil" do
      players = [
        FactoryBot.create(:player, new_quiz: "true", graded_quiz: "true"),
        FactoryBot.create(:player, new_quiz: "true", graded_quiz: "false"),
        FactoryBot.create(:player, new_quiz: "false", graded_quiz: "true"),
        FactoryBot.create(:player, new_quiz: "false", graded_quiz: "false"),
        FactoryBot.create(:player)
        ]

      Digester.remove_false_from_notifications!

      assert_equal "true", players[0].reload.new_quiz
      assert_equal "true", players[0].reload.graded_quiz
      assert_equal "true", players[1].reload.new_quiz
      assert_nil           players[1].reload.graded_quiz
      assert_nil           players[2].reload.new_quiz
      assert_equal "true", players[2].reload.graded_quiz
      assert_nil           players[3].reload.new_quiz
      assert_nil           players[3].reload.graded_quiz
    end
  end
end
