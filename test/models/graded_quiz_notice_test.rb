require 'test_helper'

class GradedQuizNoticeTest < ActiveSupport::TestCase
  class GradedQuizNoticeTest::Validations < GradedQuizNoticeTest
    def setup
      @prior = FactoryBot.create(:graded_quiz_notice)
    end

    test "a graded-quiz notice is invalid if it matches about" do
      notice = FactoryBot.build(:graded_quiz_notice, about: @prior.about,
                                 recipient: @prior.recipient)

      assert_not notice.valid?
    end

    test "a graded-quiz notice is valid if it matches only a recipient" do
      notice = FactoryBot.build(:graded_quiz_notice,
                                 recipient: @prior.recipient)

      assert notice.valid?
    end
  end

  class GradedQuizNoticeTest::Sendable < GradedQuizNoticeTest
    test "a graded-quiz notice is sendable when it has been graded and the "\
         "recipient has a confirmed address" do
      response_sheet = FactoryBot.build(:response_sheet, :scored)
      player = FactoryBot.build(:player, :with_confirmed_user)

      notice = FactoryBot.build(:graded_quiz_notice, about: response_sheet,
                                                      recipient: player)

      assert notice.sendable?
    end

    test "a graded-quiz notice is not sendable if recipient's address is not "\
         "confirmed" do
      response_sheet = FactoryBot.build(:response_sheet)
      player = FactoryBot.build(:player, :with_user)

      notice = FactoryBot.build(:graded_quiz_notice, about: response_sheet,
                                                      recipient: player)

      assert_not notice.sendable?
    end

    test "a graded-quiz notice is not sendable if has not been graded" do
      response_sheet = FactoryBot.build(:response_sheet)
      player = FactoryBot.build(:player, :with_confirmed_user)

      notice = FactoryBot.build(:graded_quiz_notice, about: response_sheet,
                                                      recipient: player)

      assert_not notice.sendable?
    end

  end

  class GradedQuizNoticeTest::Scopes < GradedQuizNoticeTest
    test ".unsent returns unsent graded-quiz notices" do
      notice = FactoryBot.create(:graded_quiz_notice, :unsent)

      assert_includes Notice::GradedQuiz.unsent, notice
    end

    test ".unsent does not return sent graded-quiz notices" do
      notice = FactoryBot.create(:graded_quiz_notice, :sent)

      assert_not_includes Notice::GradedQuiz.unsent, notice
    end
  end

  class GradedQuizNoticeTest::ShouldSend < GradedQuizNoticeTest
    test "should_send? returns true when notice is not sent" do
      notice = FactoryBot.create(:graded_quiz_notice, :unsent)

      assert notice.should_send?
    end

    test "should_send? returns false when notice is sent" do
      notice = FactoryBot.create(:graded_quiz_notice, :sent)

      assert_not notice.should_send?
    end
  end

  class GradedQuizNoticeTest::SendNotice < GradedQuizNoticeTest
    test "send_notice sends a sendable message and marks the notice sent" do
      quiz = FactoryBot.create(:quiz, :with_questions, :completed)
      player = FactoryBot.create(:player, :with_confirmed_user)
      response_sheet = FactoryBot.create(:response_sheet, :scored,
                                          quiz: quiz, player: player)
      notice = FactoryBot.create(:graded_quiz_notice, about: response_sheet,
                                  recipient: player)

      assert_difference("ActionMailer::Base.deliveries.count", 1,
                        "Message not delivered") do
        Sidekiq::Extensions::DelayedMailer.clear
        notice.send_notice
        Sidekiq::Extensions::DelayedMailer.drain
      end

      assert notice.sent_at, "Not marked sent_at"
    end

    test "send_notice doesn't send an unsendable message" do
      notice = FactoryBot.create(:graded_quiz_notice, :sent)

      assert_no_difference("ActionMailer::Base.deliveries.count",
                           "Message delivered") do
        Sidekiq::Extensions::DelayedMailer.clear
        notice.send_notice
        Sidekiq::Extensions::DelayedMailer.drain
      end
    end
  end
end
