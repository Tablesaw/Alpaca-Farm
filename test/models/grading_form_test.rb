require 'test_helper'

class GradingFormTest < ActiveSupport::TestCase

  test "New grading form creates responses from question" do
    question = FactoryBot.build(:question_with_responses,
                                 responses: ["correct", "incorrect", ""])

    grading_form = GradingForm.new(question: question)

    assert_equal question, grading_form.question, "Question was not associated"
    assert_equal 3, grading_form.responses.size, "Total count is not 3"
  end

  test "New grading form creates responses from question id" do
    question = FactoryBot.create(:question_with_responses,
                                 responses: ["correct", "incorrect", ""])

    grading_form = GradingForm.new(question_id: question.id)

    assert_equal question, grading_form.question, "Question was not associated"
    assert_equal 3, grading_form.responses.size, "Total count is not 3"
  end

  test "New grading form assigns responses hash on new" do
    question = FactoryBot.build(:question_with_responses,
                                 responses: ["correct", "incorrect", ""])
    grading_form = GradingForm.new(question: question,
                                   responses: {"KEY" => "value"})
    assert_equal "value", grading_form.responses["KEY"]
    assert_not grading_form.responses.has_key?("CORRECT"),
                    "'Correct' exists in responses"
    assert_equal 1, grading_form.responses.size, "Total count is not 1"
  end

  test "New grading form automatically marks a blank string as incorrect" do
    question = FactoryBot.create(:question_with_responses,
                                 responses: ["correct", "incorrect", ""])

    grading_form = GradingForm.new(question_id: question.id)

    assert_equal false, grading_form.responses[Question::BLANK_STRING][:correct]
    assert_nil grading_form.responses["INCORRECT"][:correct]
  end

  test "New grading form automatically marks a response as correct if it "\
       "matches the question's answer" do
    question = FactoryBot.create(:question_with_responses,
                                 responses: ["correct", "incorrect", ""],
                                 answer: "correct")

    grading_form = GradingForm.new(question_id: question.id)

    assert_equal true, grading_form.responses['CORRECT'][:correct]
    assert_nil grading_form.responses["INCORRECT"][:correct]
  end


  test "#save saves correctness of responses" do
    question = FactoryBot.create(:question_with_responses,
                                 responses: ["correct", "incorrect"])
    grading_form = GradingForm.new(question: question,
                                   responses: grading_form_params)

    grading_form.save

    assert_nil question.responses.find_by(correct: nil),
               "Unsaved responses after save"
  end

  test "#save matches square brackets to parens" do
    question = FactoryBot.create(
                 :question_with_responses,
                 responses: ["Correct [I think]", "][[][][[]]   ]"]
                 )
    grading_form = GradingForm.new(question: question,
                                   responses: bracket_params)

    grading_form.save

    assert_nil question.responses.find_by(correct: nil),
               "Unsaved responses after save"
  end

  test "#save matches BLANK_STRING to blank strings" do
    question = FactoryBot.create(:question_with_responses,
                                 responses: ["", "   "])
    grading_form = GradingForm.new(question: question,
                                   responses: {
                                     Question::BLANK_STRING => {correct: true}
                                     })

    grading_form.save

    assert_nil question.responses.find_by(correct: nil),
               "Unsaved responses after save"
  end


  private

  def grading_form_params
    {
      "CORRECT" => {correct: true},
      "INCORRECT" => {correct: false}
      }
  end

  def bracket_params
    {
      "CORRECT (I THINK)" => {correct: true},
      ")(()()(())   )" => {correct: false}
      }
  end

end
