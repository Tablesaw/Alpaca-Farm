require 'test_helper'

class QuestionTest < ActiveSupport::TestCase
  test "validates number as integer" do
    question = FactoryBot.build(:question, number: 0.2)

    assert_not question.valid?
  end

  test "validates number presence" do
    question = FactoryBot.build(:question, number: nil)

    assert_not question.valid?
  end

  test "validates question text presence on finalize" do
    question = FactoryBot.build(:question, question_text: nil)

    assert question.valid?, "Blank question text invalid with default context."
    assert_not question.valid?(:finalize), "Blank question text valid on finalize."
  end

  test "validates answer presence on finalize" do
    question = FactoryBot.build(:question, answer: nil)

    assert question.valid?, "Blank question text invalid with default context."
    assert_not question.valid?(:finalize), "Blank question text valid on finalize."
  end

  test "can calculate percent graded" do
    question = FactoryBot.create(:question_with_responses,
                                  responses: ["","",""]).
                           reload
    assert_equal (0.0).round(3), question.percent_graded.round(3)

    question.responses.first.update(correct: true)

    assert_equal (1.0/3.0 * 100).round(3), question.percent_graded.round(3)
  end

  test "can calculate percent moneyed" do
    question = FactoryBot.create(:question_with_responses,
                                  responses: ["","",""]).
                           reload
    assert_equal (0.0).round(3), question.percent_moneyed.round(3)

    question.responses.first.update(money: true)

    assert_equal (1.0/3.0 * 100).round(3), question.percent_moneyed.round(3)
  end

  test "can calculate percent correct" do
    question = FactoryBot.create(:question_with_responses,
                                  responses: ["","",""]).
                           reload
    question.responses.first.update(correct: true)
    question.responses.last.update(correct: false)

    assert_equal (1.0/2.0 * 100).round(3), question.percent_correct.round(3)
  end

  test "can update question text" do
    question = FactoryBot.build(:question, question_text: "Unedited Question")
    question.stub :save, "saved" do
      assert_not_equal "saved", question.update_question("   ")
      assert_nil question.edited_text, "Updated blank text"

      assert_not_equal "saved", question.update_question("Unedited Question")
      assert_nil question.edited_text, "Updated same info"

      assert_equal "saved", question.update_question("Edited Question")
      assert_equal "Edited Question", question.edited_text, "Did not update"
      assert_not_nil question.question_edited_at, "Did not mark time"

      edited_time = question.question_edited_at

      assert_not_equal "saved", question.update_question("Twice Edited Question")
      assert_not_equal "Twice Edited Question", question.edited_text, "Updated twice"
      assert_equal edited_time, question.question_edited_at, "Marked time twice"
    end
  end

  test "can update answer" do
    question = FactoryBot.build(:question, answer: "Correct")
    question.stub :save, "saved" do
      assert_not_equal "saved", question.update_answer("   ")
      assert_nil question.edited_answer, "Updated blank text"

      assert_not_equal "saved", question.update_answer("Correct")
      assert_nil question.edited_answer, "Updated same info"

      assert_equal "saved", question.update_answer("Mostly Correct")
      assert_equal "Mostly Correct", question.edited_answer, "Did not update"
      assert_not_nil question.answer_edited_at, "Did not mark time"

      edited_time = question.answer_edited_at

      assert_not_equal "saved", question.update_answer("Twice Edited Answer")
      assert_not_equal "Twice Edited Answer", question.edited_answer, "Updated twice"
      assert_equal edited_time, question.answer_edited_at, "Marked time twice"
    end
  end

  test "can create a hash of responses" do
    question = FactoryBot.build(:question)
    question.responses = [
      FactoryBot.build(:response, response_text: "correct"),
      FactoryBot.build(:response, response_text: "CORrect"),
      FactoryBot.build(:response, response_text: "incorrect"),
      FactoryBot.build(:response, response_text: ""),
      FactoryBot.build(:response, response_text: "   "),
      FactoryBot.build(:response, response_text: "Correct [I think]"),
      FactoryBot.build(:response, response_text: "][[][][[]]   ]")
      ]

    hash = question.responses_hash

    assert_equal 2, hash["CORRECT"][:count],
                    "'Correct' count is not 2"
    assert_equal 1, hash["INCORRECT"][:count],
                    "'Incorrect' count is not 1"
    assert_equal 2, hash[Question::BLANK_STRING][:count],
                    "Blank strings were not translated"
    assert_equal 1, hash["CORRECT (I THINK)"][:count],
                    "Brackets were not converted"
    assert_equal 1, hash[")(()()(())   )"][:count],
                    "'Incorrect' count is not 1"
    assert_equal 5, hash.size, "Total count is not 5"
  end

  test "can process a string for HTML forms" do
    test_string = "[Generic '90s Grunge Song]"

    result = Question.prepare_string(test_string)

    assert_equal "(GENERIC '90S GRUNGE SONG)", result
  end

end
