require 'test_helper'

class QuizTest < ActiveSupport::TestCase
  test "quiz accepts nested attributes for questions" do
    nested_question_count = 5
    attributes = (1..nested_question_count).map do |i|
      {
        number: i,
        question_text: "Question #{i}",
        answer: "answer #{i}"
      }
    end
    assert_difference('Question.count', nested_question_count) do
      FactoryBot.create(:quiz, :no_questions, questions_attributes: attributes)
    end
  end

  test "quiz allows destroy on nested attributes" do
    quiz = FactoryBot.create(:quiz, questions: 7)
    quiz.questions.first.mark_for_destruction
    assert_difference('Question.count', -1) do
      quiz.save
    end
  end

  test "validates author_id presence" do
    quiz = FactoryBot.build(:quiz, author: nil)

    assert_not quiz.valid?
  end

  test "validates subject presence" do
    quiz = FactoryBot.build(:quiz, subject: nil)

    assert_not quiz.valid?
  end

  test "validates base score presence" do
    quiz = FactoryBot.build(:quiz, base_score: nil)

    assert_not quiz.valid?
  end

  test "validates money question count presence" do
    quiz = FactoryBot.build(:quiz, money_question_count: nil)

    assert_not quiz.valid?
  end

  test "validates base score as integer" do
    quiz = FactoryBot.build(:quiz, base_score: 1.5)

    assert_not quiz.valid?
  end

  test "validates money question count as integer" do
    quiz = FactoryBot.build(:quiz, money_question_count: 1.5)

    assert_not quiz.valid?
  end

  test "validates base score as positive" do
    quiz = FactoryBot.build(:quiz, base_score: 0)

    assert_not quiz.valid?
  end

  test "validates money question count as positive" do
    quiz = FactoryBot.build(:quiz, money_question_count: 0)

    assert_not quiz.valid?
  end

  test "validates questions count as between 5 and 25 inclusive" do
    assert_not FactoryBot.build(:quiz, questions: 4).valid?, "4-question quiz is valid"
    assert FactoryBot.build(:quiz, questions: 5).valid?, "5-question quiz is invalid"
    assert FactoryBot.build(:quiz, questions: 25).valid?, "25-question quiz is invalid"
    assert_not FactoryBot.build(:quiz, questions: 26).valid?, "26-question quiz is valid"
  end

  test "resets money_question_count when larger than number of questions" do
    questions = 10
    quiz = FactoryBot.create(:quiz, questions: questions, money_question_count: questions)
    assert_equal 10, quiz.money_question_count, "Money count reset on valid value"

    quiz.update(money_question_count: 500)
    assert_equal 4, quiz.money_question_count, "Money count not reset on invalid value"
  end

  test "runs finalization validator on finalize" do
    quiz = FactoryBot.build(:quiz, :with_questions)

    assert quiz.valid?(:finalize),
      "Finalization validator rejecting valid object"

    quiz.questions.first.update(answer: nil)

    assert quiz.invalid?(:finalize),
      "Finalization validator accepting invalid object"
    assert quiz.valid?,
      "Model validator rejecting object on default context"
  end

  test "quiz can be started" do
    quiz = FactoryBot.build(:quiz, :with_questions)

    quiz.start_quiz(1.day.from_now)

    assert quiz.opened
  end

  test "starting quiz increments authored quizzes count" do
    quiz = FactoryBot.build(:quiz, :with_questions)

    assert_difference("quiz.author.opened_quizzes_count") do
      quiz.start_quiz(1.day.from_now)
    end
  end

  test "defaults can be set" do
    quiz = Quiz.new
    quiz.set_defaults
    assert_equal 15, quiz.base_score,
                 "Base score is incorrect"
    assert_equal 5, quiz.money_question_count,
                 "Money question count is incorrect"
    assert_equal 12, quiz.questions.size,
                 "Number of questions is incorrect"
  end

  test "quiz can be closed for grading" do
    quiz = FactoryBot.build(:quiz, :ready_for_grading)

    quiz.close_quiz!

    assert_not quiz.playable?, "Quiz is still playable"
    assert_instance_of ActiveSupport::TimeWithZone, quiz.closing_date_time,
      "Closing time not set"
  end

  test "can calculate percent of responses graded" do
    questions = 5
    quiz = FactoryBot.build(:quiz, :with_answered_questions, questions: questions)
    assert_equal (0.0).round(3), quiz.percent_graded.round(3)

    quiz.questions.first.stub :percent_graded, 10.0 do
      assert_equal (0.1/questions.to_f * 100).round(3), quiz.percent_graded.round(3)
    end
  end

end
