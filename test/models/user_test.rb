require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # def setup
  #   @user1 = users(:user1)
  #   @user2 = users(:user2)
  # end

  test "user accepts nested attributes for player" do
    assert_difference('Player.count') do
      FactoryBot.create(:user, player_attributes: {name: "Example"})
    end
  end

end
