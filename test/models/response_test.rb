require 'test_helper'

class ResponseTest < ActiveSupport::TestCase
  test "validate money presence" do
    response = FactoryBot.build(:response, money: nil)

    assert_not response.valid?
  end
end
