require 'test_helper'

class CompletedQuizNoticeTest < ActiveSupport::TestCase
  class CompletedQuizNoticeTest::Validations < CompletedQuizNoticeTest
    def setup
      @prior = FactoryBot.create(:completed_quiz_notice)
    end

    test "a completed-quiz notice is invalid if it matches about and recipient" do
      notice = FactoryBot.build(:completed_quiz_notice, about: @prior.about,
                                 recipient: @prior.recipient)

      assert_not notice.valid?
    end

    test "a completed-quiz notice is valid if it matches only an about" do
      notice = FactoryBot.build(:completed_quiz_notice, about: @prior.about)

      assert notice.valid?
    end

    test "a completed-quiz notice is valid if it matches only a recipient" do
      notice = FactoryBot.build(:completed_quiz_notice,
                                 recipient: @prior.recipient)

      assert notice.valid?
    end
  end

  class CompletedQuizNoticeTest::Sendable < CompletedQuizNoticeTest
    test "a completed-quiz notice is sendable when it is completed and the "\
         "recipient has a confirmed address" do
      quiz = FactoryBot.build(:quiz, :completed)
      player = FactoryBot.build(:player, :with_confirmed_user)

      notice = FactoryBot.build(:completed_quiz_notice, about: quiz,
                                                         recipient: player)

      assert notice.sendable?
    end

    test "a completed-quiz notice is not sendable if the recipient's address "\
         "is not confirmed" do
      quiz = FactoryBot.build(:quiz, :closed_for_grading)
      player = FactoryBot.build(:player, :with_user)

      notice = FactoryBot.build(:completed_quiz_notice, about: quiz,
                                                         recipient: player)

      assert_not notice.sendable?
    end

    test "a completed-quiz notice is not sendable if it is being graded" do
      quiz = FactoryBot.build(:quiz, :closed_for_grading)
      player = FactoryBot.build(:player, :with_confirmed_user)

      notice = FactoryBot.build(:completed_quiz_notice, about: quiz,
                                                         recipient: player)

      assert_not notice.sendable?
    end

  end

  class CompletedQuizNoticeTest::Scopes < CompletedQuizNoticeTest
    test ".unsent returns unsent completed-quiz notices" do
      notice = FactoryBot.create(:completed_quiz_notice, :unsent)

      assert_includes Notice::CompletedQuiz.unsent, notice
    end

    test ".unsent does not return sent completed-quiz notices" do
      notice = FactoryBot.create(:completed_quiz_notice, :sent)

      assert_not_includes Notice::CompletedQuiz.unsent, notice
    end
  end

  class CompletedQuizNoticeTest::ShouldSend < CompletedQuizNoticeTest
    test "should_send? returns true when notice is not sent" do
      notice = FactoryBot.create(:completed_quiz_notice, :unsent)

      assert notice.should_send?
    end

    test "should_send? returns false when notice is sent" do
      notice = FactoryBot.create(:completed_quiz_notice, :sent)

      assert_not notice.should_send?
    end
  end

  # class CompletedQuizNoticeTest::SendNotice < CompletedQuizNoticeTest
  #   test "send_notice sends a sendable message and marks the notice sent" do
  #     quiz = FactoryBot.create(:quiz, :with_questions, :completed)
  #     player = FactoryBot.create(:player, :with_user)
  #     FactoryBot.create(:response_sheet, :scored,
  #                        quiz: quiz, player: player)
  #     notice = FactoryBot.create(:completed_quiz_notice, about: quiz,
  #                                 recipient: player)

  #     assert_difference("ActionMailer::Base.deliveries.count", 1,
  #                       "Message not delivered") do
  #       Sidekiq::Extensions::DelayedMailer.clear
  #       notice.send_notice
  #       Sidekiq::Extensions::DelayedMailer.drain
  #     end

  #     assert notice.sent_at, "Not marked sent_at"
  #   end

  #   test "send_notice doesn't send an unsendable message" do
  #     notice = FactoryBot.create(:completed_quiz_notice, :sent)

  #     assert_no_difference("ActionMailer::Base.deliveries.count",
  #                          "Message delivered") do
  #       Sidekiq::Extensions::DelayedMailer.clear
  #       notice.send_notice
  #       Sidekiq::Extensions::DelayedMailer.drain
  #     end
  #   end
  # end
end
