require 'test_helper'

class ResponseSheetTest < ActiveSupport::TestCase
  test "response sheet accepts nested attributes for responses" do
    questions = 7
    quiz = FactoryBot.create(:quiz_with_questions, questions: questions)

    assert_difference('Response.count', questions) do
      FactoryBot.create(:response_sheet, quiz: quiz,
                         responses_attributes: valid_responses(quiz))
    end
  end

  test "create_responses builds responses equal to questions" do
    quiz = FactoryBot.build(:quiz_with_questions)

    response_sheet = quiz.response_sheets.new
    response_sheet.create_responses
    assert_equal quiz.questions.size, response_sheet.responses.size
  end

  test "response_sheet without associated quiz is invalid" do
    response_sheet = FactoryBot.build(:response_sheet, quiz: nil)
    assert response_sheet.invalid?
  end

  test "response sheet without associated parent is invalid" do
    response_sheet = FactoryBot.build(:response_sheet, player: nil)
    assert response_sheet.invalid?
  end

  test "response sheet cannot have more responses than its quiz has questions" do
    response_sheet = FactoryBot.build(:response_sheet, :with_responses)
    response_sheet.responses.new money: false

    assert response_sheet.invalid?
  end

  test "response sheet cannot have less responses than its quiz has questions" do
    response_sheet = FactoryBot.build(:response_sheet, :with_responses)
    response_sheet.responses.destroy(response_sheet.responses.first)

    assert response_sheet.invalid?
  end

  test "response sheet cannot have more than required moneys" do
    response_sheet = FactoryBot.build(:response_sheet, :with_responses)
    response_sheet.responses.last.money = true

    assert response_sheet.invalid?
  end

  test "response sheet cannot have less than required moneys" do
    response_sheet = FactoryBot.build(:response_sheet, :with_responses)
    response_sheet.responses.first.money = false

    assert response_sheet.invalid?
  end

  private

  def valid_responses(quiz)
    quiz.questions.map.with_index do |question, i|
      {question_id: question.id, money: i < 5, response_text: "Response Text"}
    end
  end
end
