require 'test_helper'

class PlayerTest < ActiveSupport::TestCase

  test "validates name presence" do
    player = FactoryBot.build(:player, name: nil)

    assert_not player.valid?
  end

  test "validates name length between 3 and 20 characters inclusive" do
    player = FactoryBot.build(:player, name: "12")
    assert_not player.valid?, "2 character name is valid"

    player.name = "123"
    assert player.valid?, "3 character name is invalid"

    player.name = "12345678901234567890"
    assert player.valid?, "20 character name is invalid"

    player.name = "123456789012345678901"
    assert_not player.valid?, "21 character name is valid"
  end

  test "validates name format as letters, numbers, and hyphens only" do
    player = FactoryBot.build(:player, name: "Aa0-")
    assert player.valid?, "'Aa0-' is invalid"

    player.name = "A A"
    assert_not player.valid?, "Name with spaces is valid"

    player.name = "@@@"
    assert_not player.valid?, "Name with at signs is valid"
  end

  test "validates name uniqueness regardless of letter case" do
    FactoryBot.create(:player, name: "ALPACA")

    player = FactoryBot.build(:player, name: "alpaca")

    assert_not player.valid?
  end

end
