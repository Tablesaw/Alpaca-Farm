require 'test_helper'

class NewQuizNoticeTest < ActiveSupport::TestCase
  class NewQuizNoticeTest::Validations < NewQuizNoticeTest
    def setup
      @prior = FactoryBot.create(:new_quiz_notice)
    end

    test "a new quiz notice is invalid if it matches about and recipient" do
      notice = FactoryBot.build(:new_quiz_notice, about: @prior.about,
                                 recipient: @prior.recipient)

      assert_not notice.valid?
    end

    test "a new quiz notice is valid if it matches only an about" do
      notice = FactoryBot.build(:new_quiz_notice, about: @prior.about)

      assert notice.valid?
    end

    test "a new quiz notice is valid if it matches only a recipient" do
      notice = FactoryBot.build(:new_quiz_notice, recipient: @prior.recipient)

      assert notice.valid?
    end
  end

  class NewQuizNoticeTest::Sendable < NewQuizNoticeTest
    test "a new-quiz notice is sendable when it is active and ineligible for "\
         "grading and when recipient's address is confirmed" do
      quiz = FactoryBot.build(:quiz, :opened, :uncloseable)
      player = FactoryBot.build(:player, :with_confirmed_user)

      notice = FactoryBot.build(:new_quiz_notice, about: quiz,
                                                   recipient: player)

      assert notice.sendable?
    end

    test "a new-quiz notice is not sendable when the recipient's address is "\
         "unconfirmed" do
      quiz = FactoryBot.build(:quiz)
      player = FactoryBot.build(:player, :with_user)

      notice = FactoryBot.build(:new_quiz_notice, about: quiz,
                                                   recipient: player)
      assert_not notice.sendable?
    end

    test "a new-quiz notice is not sendable when it is unopened" do
      quiz = FactoryBot.build(:quiz)
      player = FactoryBot.build(:player, :with_confirmed_user)

      notice = FactoryBot.build(:new_quiz_notice, about: quiz,
                                                   recipient: player)
      assert_not notice.sendable?
    end

    test "a new-quiz notice is not sendable if it is closed" do
      quiz = FactoryBot.build(:quiz, :closed_for_grading)
      player = FactoryBot.build(:player, :with_confirmed_user)

      notice = FactoryBot.build(:new_quiz_notice, about: quiz,
                                                   recipient: player)

      assert_not notice.sendable?
    end

    test "a new-quiz notice is not sendable when it is eligible for grading" do
      quiz = FactoryBot.build(:quiz, :opened, :closeable)
      player = FactoryBot.build(:player, :with_confirmed_user)

      notice = FactoryBot.build(:new_quiz_notice, about: quiz,
                                                   recipient: player)

      assert_not notice.sendable?
    end
  end

  class NewQuizNoticeTest::Scopes < NewQuizNoticeTest
    test ".unsent returns unsent new-quiz notices" do
      notice = FactoryBot.create(:new_quiz_notice, :unsent)

      assert_includes Notice::NewQuiz.unsent, notice
    end

    test ".unsent does not return sent new-quiz notices" do
      notice = FactoryBot.create(:new_quiz_notice, :sent)

      assert_not_includes Notice::NewQuiz.unsent, notice
    end
  end

  class NewQuizNoticeTest::ShouldSend < NewQuizNoticeTest
    test "should_send? returns true when notice is not sent" do
      notice = FactoryBot.create(:new_quiz_notice, :unsent)

      assert notice.should_send?
    end

    test "should_send? returns false when notice is sent" do
      notice = FactoryBot.create(:new_quiz_notice, :sent)

      assert_not notice.should_send?
    end
  end

  class NewQuizNoticeTest::SendNotice < NewQuizNoticeTest
    test "send_notice sends a sendable message and marks the notice sent" do
      notice = FactoryBot.create(:new_quiz_notice)

      assert_difference("ActionMailer::Base.deliveries.count", 1,
                        "Message not delivered") do
        Sidekiq::Extensions::DelayedMailer.clear
        notice.send_notice
        Sidekiq::Extensions::DelayedMailer.drain
      end

      assert notice.sent_at, "Not marked sent_at"
    end

    test "send_notice doesn't send an unsendable message" do
      notice = FactoryBot.create(:new_quiz_notice, :sent)

      assert_no_difference("ActionMailer::Base.deliveries.count",
                        "Message delivered") do
        Sidekiq::Extensions::DelayedMailer.clear
        notice.send_notice
        Sidekiq::Extensions::DelayedMailer.drain
      end
    end
  end
end
