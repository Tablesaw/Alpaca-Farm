require 'test_helper'

class GradingControllerTest < ActionController::TestCase
  test "Author can close quiz for grading" do
    user = FactoryBot.create(:confirmed_user)
    quiz = FactoryBot.create(:quiz, :ready_for_grading, author: user.player)

    sign_in user
    post :start, quiz_id: quiz.id
    assert_redirected_to grading_path(quiz)
  end

  test "Author can view grading overview" do
    user = FactoryBot.create(:confirmed_user)
    quiz = FactoryBot.create(:quiz, :closed_for_grading, :with_questions, author: user.player)
    sign_in user

    get :show, quiz_id: quiz.id

    assert :success
  end

  test "Author can view question grading" do
    user = FactoryBot.create(:confirmed_user)
    quiz = FactoryBot.create(:quiz, :closed_for_grading, :with_questions, author: user.player)
    sign_in user

    get :question, {quiz_id: quiz.id,
                    question_number: quiz.questions.first.number}

    assert :success
  end

  test "Author can save question grading information" do
    user = FactoryBot.create(:confirmed_user)
    quiz = FactoryBot.create(:quiz, :closed_for_grading, :with_questions, author: user.player)
    question = quiz.questions.first
    sign_in user

    post :save_grading, quiz_id: quiz.id,
                        question_number: question.number,
                        grading_form: grading_form_params(question)

    assert_nil question.responses.find_by(correct: nil),
               "Unsaved responses after save"
  end

  test "Author can mark grading complete" do
    user = FactoryBot.create(:confirmed_user)
    quiz = FactoryBot.create(:quiz, :fully_graded, author: user.player)
    sign_in user

    assert_difference("GradedQuizWorker.jobs.size", 1,
                      "GradedQuizWorker not called") do
      post :complete, quiz_id: quiz.id
    end

    assert_redirected_to quiz, "Not redirected"
    assert_equal "This quiz is complete!", flash[:notice], "Wrong flash"
    quiz = assigns[:quiz]

    assert_not_nil quiz.questions.first.money_value,
                 "Money values not calculated"
    assert_not_nil quiz.maximum_score,
                 "Maximum score not calculated"
    assert_not_nil quiz.response_sheets.first.score,
                 "Score not calculated"
    assert_not_nil quiz.response_sheets.first.rank,
                 "Rank not calculated"
    assert_not_nil quiz.response_sheets.first.percentile_rank,
                 "Percentile rank not calculated"
    assert_not_nil quiz.response_sheets.first.percent_of_champion,
                 "Percent_of_champion not calculated"
  end

  test "Must be logged in to grade" do
    quiz = FactoryBot.create(:quiz, :ready_for_grading, :with_questions)

    post :start, quiz_id: quiz.id
    assert_redirected_to new_user_session_path, "start grading"

    quiz = FactoryBot.create(:quiz, :closed_for_grading, :with_questions)

    get :show, quiz_id: quiz.id
    assert_redirected_to new_user_session_path, "show grading summary"

    get :question, {quiz_id: quiz.id,
                    question_number: quiz.questions.first.number}
    assert_redirected_to new_user_session_path, "show grade question"

    post :save_grading, {quiz_id: quiz.id,
                     question_number: quiz.questions.first.number}
    assert_redirected_to new_user_session_path, "saving grade question info"

    post :complete, quiz_id: quiz.id
    assert_redirected_to new_user_session_path, "complete grading"
  end

  test "Must be author to grade" do
    user = FactoryBot.create(:confirmed_user)
    sign_in user
    quiz = FactoryBot.create(:quiz, :ready_for_grading, :with_questions)

    post :start, quiz_id: quiz
    assert_redirected_to quiz, "wrong redirect on start grading"
    assert_equal nonauthor_message,
                 flash[:alert], "wrong method on start grading"

    quiz = FactoryBot.create(:quiz, :closed_for_grading, :with_questions)

    get :show, quiz_id: quiz.id
    assert_redirected_to quiz,
                         "wrong redirect on show grading summary"
    assert_equal nonauthor_message,
                 flash[:alert], "wrong method on show grading summary"

    get :question, {quiz_id: quiz.id,
                    question_number: quiz.questions.first.number}
    assert_redirected_to quiz,
                         "wrong redirect on show grade question"
    assert_equal nonauthor_message,
                 flash[:alert], "wrong method on show grade question"

    post :save_grading, {quiz_id: quiz.id,
                     question_number: quiz.questions.first.number}
    assert_equal nonauthor_message,
                 flash[:alert], "wrong method on save grade question info"

    post :complete, quiz_id: quiz.id
    assert_redirected_to quiz,
                         "wrong redirect on complete grading"
    assert_equal nonauthor_message,
                 flash[:alert], "wrong method on complete grading"
  end

  private

  def nonauthor_message
    "This quiz is closed for grading."
  end

  def grading_form_params(question)
    {question_id: question.id,
      responses: {
        "CORRECT" => {correct: true},
        "INCORRECT" => {correct: false},
        Question::BLANK_STRING => {correct: false}
        }
      }
  end

end
