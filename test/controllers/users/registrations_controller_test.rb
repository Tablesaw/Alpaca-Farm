require 'test_helper'

class UsersRegistrationsControllerTest < ActionController::TestCase
  setup do
    @controller = Users::RegistrationsController.new
    @request.env["devise.mapping"] = Devise.mappings[:user]
  end

  test "get users/sign_up builds player" do
    get :new
    user = assigns(:user)
    assert user.player
  end

  test "can get new" do
    get :new
    assert_response :success
  end

  test "can create user and player" do
    assert_difference('User.count') do
      assert_difference('Player.count') do
        post :create, user: user_params
      end
    end
  end

  test "no user created on invalid player name" do
    FactoryBot.create(:player, name: "UniqueName")
    invalid_params = user_params
    invalid_params[:player_attributes][:name] = "UniqueName"

    assert_no_difference('User.count') do
      assert_no_difference('Player.count') do
        post :create, user: invalid_params
      end
    end
  end


  private

  def user_params
    {email: "email@example.com",
      password: "password",
      password_confirmation: "password",
      player_attributes: {name: "Example"}}
  end

end
