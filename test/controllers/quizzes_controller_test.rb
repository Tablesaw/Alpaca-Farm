require 'test_helper'

class QuizzesControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  test "can get index" do
    get :index
    assert_response :success
  end


  test "can get home page" do
    FactoryBot.create(:quiz, :opened, :uncloseable)
    FactoryBot.create(:quiz, :closed_for_grading)
    FactoryBot.create(:quiz, :completed)

    get :home_page
    assert_response :success
    assert_not_nil assigns(:playable_quizzes), "No playable quizzes"
    assert_not_nil assigns(:quizzes_being_graded), "No quizzes being graded"
    assert_not_nil assigns(:complete_quizzes), "No complete quizzes"
  end

  test "signed in user can get new" do
    user = FactoryBot.create(:user)
    sign_in user

    get :new
    assert_response :success
  end

  test "cannot get new without sign in" do
    get :new
    assert_redirected_to new_user_session_path
  end

  test "new builds twelve questions" do
    user = FactoryBot.create(:user)
    sign_in user

    get :new
    quiz = assigns(:quiz)

    assert_equal 12, quiz.questions.size
  end

  test "can create quiz" do
    user = FactoryBot.create(:user)
    sign_in user

    assert_difference('Quiz.count') do
      post :create, quiz: quiz_test_params
    end

    assert_redirected_to quiz_path(assigns(:quiz))
  end

  test "only show closed or playable quizzes without sign in" do

    get :show, id: FactoryBot.create(:quiz, :completed).id
    assert_response :success, "Redirected on complete quiz"

    get :show, id: FactoryBot.create(:quiz, :closed_for_grading).id
    assert_template :show, partial: "_review",
                    message: "No review when being graded"

    get :show, id: FactoryBot.create(:quiz, :opened).id
    # assert_redirected_to new_user_session_path,
    #                      "Playable quiz shown without sign-in"
    assert_template "show", partial: "_response_form"

    get :show, id: FactoryBot.create(:quiz).id
    assert_redirected_to new_user_session_path,
                         "Unready quiz shown without sign-in"
  end

  test "author can view unready quiz" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.create(:quiz, author: user.player)
    sign_in user

    get :show, id: quiz.id
    assert :success
  end

  test "non-author cannot view unready quiz" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.create(:quiz)
    sign_in user

    get :show, id: quiz
    assert_redirected_to root_path
  end

  test "non-author can get response form for playable" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.create(:playable_quiz)
    sign_in user

    get :show, id: quiz
    assert_template "show", partial: "_response_form"
  end

  test "non-author can review previous response while playable" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.create(:playable_quiz)
    FactoryBot.create(:response_sheet, :with_responses,
                       quiz: quiz, player: user.player)
    sign_in user

    get :show, id: quiz
    assert_template "show", partial: "_review"
  end

  test "author can review quiz while playable" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.create(:quiz, :opened, :uncloseable, author: user.player)
    sign_in user

    get :show, id: quiz.id
    assert_template "show", partial: "_review"
  end

  test "author redirected to grading when quiz is closed for grading" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.create(:quiz, :closed_for_grading, author: user.player)
    sign_in user

    get :show, id: quiz.id
    assert_redirected_to grading_path(quiz)
  end

  test "non-author can review quiz while being graded" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.create(:quiz, :closed_for_grading)
    sign_in user

    get :show, id: quiz.id
    assert_template "show", partial: "_review"
  end


  test "author can get edit" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.create(:quiz, author: user.player)
    sign_in user

    get :edit, id: quiz.id

    assert_response :success
  end

  test "non-author cannot get edit" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.create(:quiz)

    get :edit, id: quiz.id
    assert_redirected_to root_path,
      "Edit rendered without signin"
    assert_equal unauthorized_alert_message, flash[:alert],
                 "No alert sent with no sign in"

    sign_in user
    get :edit, id: quiz.id
    assert_redirected_to root_path,
      "Edit rendered for non-author"
    assert_equal unauthorized_alert_message, flash[:alert],
                 "No alert sent for non-author"

  end

  test "user can update authored quiz" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.create(:quiz, author: user.player)
    sign_in user

    patch :update, id: quiz.id, quiz: quiz_test_params
    quiz_result = assigns[:quiz]

    assert_redirected_to quiz_path(quiz), "Did not redirect"
    assert_equal quiz_test_params[:subject], quiz_result.subject, "Did not update"
  end

  test "non-authors cannot update quiz" do
    quiz = FactoryBot.create(:quiz)

    patch :update, id: quiz.id, quiz: quiz_test_params
    quiz_result = assigns[:quiz]

    assert_not_equal quiz_test_params[:subject], quiz_result.subject,
      "Updated without signin"

    sign_in FactoryBot.create(:user)

    patch :update, id: quiz.id, quiz: quiz_test_params
    quiz_result = assigns[:quiz]

    assert_not_equal quiz_test_params[:subject], quiz_result.subject,
      "Updated by non-author"
  end

  test "author can destroy quiz" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.create(:quiz, author: user.player)
    sign_in user

    assert_difference('Quiz.count', -1) do
      delete :destroy, id: quiz.id
    end

    assert_redirected_to quizzes_path
  end


  test "non-authors cannot destroy quiz" do
    quiz = FactoryBot.create(:quiz)

    assert_no_difference('Quiz.count', "Destroyed without sign in") do
      delete :destroy, id: quiz.id
    end
    assert_redirected_to root_path, "Redirected to home page without sign in"

    sign_in FactoryBot.create(:user)

    assert_no_difference('Quiz.count', "Destroyed by other user") do
      delete :destroy, id: quiz.id
    end
    assert_redirected_to root_path, "Redirected to home page for non-author"
  end

  test "author can get finalization page" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.create(:quiz_with_questions, author: user.player)
    sign_in user

    get :finalize, id: quiz.id
    assert_response :success
  end

  test "author cannot get finalization page on active quiz" do
    user = FactoryBot.create(:user)
    opened_quiz = FactoryBot.create(:quiz, :opened, author: user.player)
    completed_quiz = FactoryBot.create(:quiz, :completed, author: user.player)
    closed_quiz = FactoryBot.create(:quiz, :closed_for_grading, author: user.player)
    sign_in user

    get :finalize, id: completed_quiz.id
    assert_redirected_to quiz_path(completed_quiz),
      "Got finalization for completed quiz"

    get :finalize, id: closed_quiz.id
    assert_redirected_to quiz_path(closed_quiz),
      "Got finalization for quiz being graded"

    get :finalize, id: opened_quiz.id
    assert_redirected_to quiz_path(opened_quiz),
      "Got finalization for playable quiz"
  end


  test "non-authors cannot get finalization_page" do
    quiz = FactoryBot.create(:quiz_with_questions)

    get :finalize, id: quiz.id
    assert_redirected_to root_path, "Finalize rendered without signin"

    sign_in FactoryBot.create(:user)

    get :finalize, id: quiz.id
    assert_redirected_to root_path, "Finalize rendered for non-author"
  end

  test "author can start quiz" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.create(:quiz_with_questions, author: user.player)
    sign_in user

    post :start_quiz, id: quiz.id,
                      quiz: {approximate_grading_date_time: 1.day.from_now }

    assert_redirected_to root_path, "Author redirected incorrectly"
    assert_equal "Your quiz is now live!", flash[:notice],
                 "No notice sent after quiz started"
  end

  test "author cannot start active quiz" do
    user = FactoryBot.create(:user)
    opened_quiz = FactoryBot.create(:quiz, :opened, author: user.player)
    completed_quiz = FactoryBot.create(:quiz, :completed, author: user.player)
    closed_quiz = FactoryBot.create(:quiz, :closed_for_grading, author: user.player)
    sign_in user

    get :start_quiz, id: completed_quiz.id
    assert_redirected_to completed_quiz, "Incorrect redirect for completed quiz"
    assert_equal check_unready_message, flash[:alert],
                 "Incorrect alert for completed quiz"

    get :finalize, id: closed_quiz.id
    assert_redirected_to closed_quiz, "Incorrect redirect for quiz being graded"
    assert_equal check_unready_message, flash[:alert],
                 "Incorrect alert for quiz being graded"

    get :finalize, id: opened_quiz.id
    assert_redirected_to opened_quiz, "Incorrect redirect for playable quiz"
    assert_equal check_unready_message, flash[:alert],
                 "Incorrect alert for playable quiz"
  end

  test "non-author cannot start quiz" do
    future_time = 1.day.from_now
    quiz = FactoryBot.create(:quiz_with_questions)

    post :start_quiz, id: quiz.id,
                      quiz: {approximate_grading_date_time: future_time }
    assert_redirected_to root_path
    assert_equal unauthorized_alert_message, flash[:alert],
                 "No alert sent with no sign in"

    sign_in FactoryBot.create(:user)

    post :start_quiz, id: quiz.id,
                      quiz: {approximate_grading_date_time: future_time }
    assert_redirected_to root_path
    assert_equal unauthorized_alert_message, flash[:alert],
                 "No alert sent with no sign in"

  end


  test "cannot start invalid quiz" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.build(:quiz, author: user.player)
    quiz.questions[0].answer = ""
    quiz.save
    sign_in user

    post :start_quiz, id: quiz.id,
                      quiz: {approximate_grading_date_time: 1.day.from_now }
    assert_template :edit
  end

  test "Grading time cannot be less than eight hours from now" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.create(:quiz_with_questions, author: user.player)
    sign_in user

    future_time = 1.hour.from_now
    post :start_quiz, id: quiz.id,
                      quiz: {approximate_grading_date_time: future_time }
    assert_redirected_to finalize_path(quiz)
    assert_equal "Quizzes must be open for at least eight hours.",
      flash[:alert]
  end


  private
evens = (1..10).each_with_object([]) { |i, a| a << i*2 }

  def quiz_test_params
    {base_score: 15,
      money_question_count: 5,
      subject: "New subject for testing",
      description: "New Test Description for testing",
      questions_attributes: (1..5).each_with_object({}) do |i, hash|
          hash["#{i-1}"] = {
            number: 1
          }
        end
    }
  end

  def unauthorized_alert_message
    "You are not authorized to view or edit this quiz."
  end

  def check_unready_message
    "This quiz has already been started."
  end

end
