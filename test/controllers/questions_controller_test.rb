require 'test_helper'

class QuestionsControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  test "question page for completed quiz is viewable without signin" do
    quiz = FactoryBot.build(:quiz, :completed)
    question = FactoryBot.create(:question, quiz: quiz)

    get :show, id: question.id
    assert_response :success
  end

  test "question page for in-progress quiz cannot be viewed by non-author" do
    quiz = FactoryBot.build(:quiz, :opened)
    question = FactoryBot.create(:question, quiz: quiz)

    get :show, id: question.id
    assert_redirected_to quiz_path(quiz),
      "question page shown without sign-in"

    user = FactoryBot.create(:user)
    sign_in user

    get :show, id: question.id
    assert_redirected_to quiz_path(quiz),
      "question page shown with wrong sign-in"
  end

  test "question page is viewable by authors before quiz is completed" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.build(:quiz, :opened, author: user.player)
    question = FactoryBot.create(:question, quiz: quiz)

    sign_in user
    get :show, id: question.id
    assert_response :success
  end

  test "question page assigns responses hash for author" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.build(:quiz, :completed, author: user.player)
    question = FactoryBot.create(:question_with_responses, quiz: quiz)

    sign_in user
    get :show, id: question.id
    assert assigns[:responses_hash]
  end

  test "responses hash is not assigned for non-authors" do
    quiz = FactoryBot.build(:quiz, :completed)
    question = FactoryBot.create(:question, quiz: quiz)

    get :show, id: question.id
    assert_not assigns[:responses_hash],
      "responses hash assigned without sign-in"

    user = FactoryBot.create(:user)
    sign_in user

    get :show, id: question.id
    assert_not assigns[:responses_hash],
      "responses hash assigned wrong sign-in"
  end

  test "unsigned_in user cannot use non-show actions" do
    quiz = FactoryBot.build(:quiz, :completed)
    question = FactoryBot.create(:question, quiz: quiz)

    get :edit_question, id: question.id
    assert_redirected_to new_user_session_path, "accessed edit_question"

    get :edit_answer, id: question.id
    assert_redirected_to new_user_session_path, "accessed edit_answer"

    put :update, id: question.id
    assert_redirected_to new_user_session_path, "accessed update"
  end

  test "signed-in non-author cannot edit questions" do
    quiz = FactoryBot.build(:quiz, :completed)
    question = FactoryBot.create(:question, quiz: quiz)
    user = FactoryBot.create(:user)
    sign_in user

    get :edit_question, id: question.id
    assert_redirected_to quiz_path(quiz), "accessed edit_question"

    get :edit_answer, id: question.id
    assert_redirected_to quiz_path(quiz), "accessed edit_answer"

    put :update, id: question.id
    assert_redirected_to quiz_path(quiz), "accessed update"
  end

  test "author cannot edit individual questions before opening" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.build(:quiz, author: user.player)
    question = FactoryBot.create(:question, quiz: quiz)
    sign_in user

    get :edit_question, id: question.id
    assert_redirected_to quiz_path(quiz), "accessed edit_question"

    get :edit_answer, id: question.id
    assert_redirected_to quiz_path(quiz), "accessed edit_answer"

    put :update, id: question.id
    assert_redirected_to quiz_path(quiz), "accessed update"
  end

  test "author can edit question text" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.build(:quiz, :opened, author: user.player)
    question = FactoryBot.create(:question, quiz: quiz)
    sign_in user

    get :edit_question, id: question.id

    assert_response :success
  end

  test "author can edit answer text" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.build(:quiz, :opened, author: user.player)
    question = FactoryBot.create(:question, quiz: quiz)
    sign_in user

    get :edit_answer, id: question.id

    assert_response :success
  end

  test "author can submit params to update question" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.build(:quiz, :opened, author: user.player)
    question = FactoryBot.create(:question, quiz: quiz)
    sign_in user

    put :update, { id: question.id,
                   edit_type: "question",
                   question: {new_text: "new text"}
                   }


    assert_redirected_to question_path(question)
    assert_equal "new text", question.reload.edited_text
  end

  test "author can submit params to update answer" do
    user = FactoryBot.create(:user)
    quiz = FactoryBot.build(:quiz, :opened, author: user.player)
    question = FactoryBot.create(:question, quiz: quiz)
    sign_in user

    put :update, { id: question.id,
                   edit_type: "answer",
                   question: {new_answer: "new answer"}
                   }


    assert_redirected_to question_path(question)
    assert_equal "new answer", question.reload.edited_answer
  end
end
