require 'test_helper'

class PlayersControllerTest < ActionController::TestCase
  test "should get index" do
    FactoryBot.create_list(:player, 2)

    get :index
    assert_response :success
    assert_not_nil assigns(:players)
  end

  test "should show player" do
    player = FactoryBot.create(:player)
    get :show, id: player.id
    assert_response :success
  end

  test "player can get edit for their own record" do
    player = FactoryBot.create(:player, :with_user)
    sign_in player.user
    get :edit, id: player.id
    assert_response :success
  end

  test "others cannot get edit for a player" do
    player = FactoryBot.create(:player)

    get :edit, id: player
    assert_redirected_to player_path(player), "Edit rendered without sign in"

    user = FactoryBot.create(:user)
    sign_in user

    get :edit, id: player
    assert_redirected_to player_path(player), "Edit rendered for other user"
  end

  test "player can update their own record" do
    player = FactoryBot.create(:player, :with_user)
    sign_in player.user

    patch :update, id: player.id, player: player_params
    assert_redirected_to player_path(player)
    assert flash[:notice]
    assert_equal player_params[:location], assigns[:player][:location]
  end

  test "others cannot update a player" do
    player = FactoryBot.create(:player)

    patch :update, id: player.id, player: player_params
    assert flash[:alert]

    user = FactoryBot.create(:user)
    sign_in user
    patch :update, id: player.id, player: player_params
    assert flash[:alert]
  end

  test "'nil' strings in whitelisted params are saved as nil value" do
    player = FactoryBot.create(:player, :with_user,
                                new_quiz: "daily", graded_quiz: "immediately",
                                motto: "Test your code")
    sign_in player.user

    patch :update, id: player.id, player: {new_quiz: "nil", graded_quiz: "nil",
                                           motto: "nil"}

    player.reload
    assert_nil player.new_quiz, "new_quiz not saved as nil"
    assert_nil player.graded_quiz, "graded_quiz not saved as nil"
    assert_not_nil player.motto, "non-whitelisted param saved as nil"
   end

  test "nil values in specified attributes changed to 'nil' string on edit" do
    player = FactoryBot.create(:player, :with_user,
                                new_quiz: nil, graded_quiz: nil,
                                motto: nil)
    sign_in player.user

    get :edit, id: player.id

    notifications = assigns[:player][:notifications]
    assert_equal "nil", notifications["new_quiz"], "new_quiz not stringified"
    assert_equal "nil", notifications["graded_quiz"], "graded_quiz not stringified"
    assert_nil assigns[:player][:motto], "non-whitelisted param stringified"
  end

  private

  def player_params
    {location: "Testerville", motto: "Always test your code!",
                              time_zone: "Central Time (US & Canada)"}
  end

end
