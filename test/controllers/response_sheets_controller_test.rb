require 'test_helper'

class ResponseSheetsControllerTest < ActionController::TestCase
  test "player can create response sheet" do
    questions = 7
    quiz = FactoryBot.create(:playable_quiz, questions: 7)
    user = FactoryBot.create(:confirmed_user)

    assert_difference('ResponseSheet.count') do
      assert_difference('Response.count', questions) do
        sign_in user
        post :create, response_sheet: rs_params(quiz)
      end
    end

    assert_equal successful_create_message, flash[:notice],
                 "Incorrect notice message"
  end

  test "Response sheet cannot be created without signin" do
    quiz = FactoryBot.create(:playable_quiz)

    assert_no_difference('ResponseSheet.count') do
      assert_no_difference('Response.count') do
        post :create, response_sheet: rs_params(quiz)
      end
    end

    assert_redirected_to new_user_session_path, "Not redirected to login"
  end

  test "Response sheet cannot be created by author" do
    user = FactoryBot.create(:confirmed_user)
    quiz = FactoryBot.create(:playable_quiz, author: user.player)

    assert_no_difference('ResponseSheet.count') do
      assert_no_difference('Response.count') do
        sign_in user
        post :create, response_sheet: rs_params(quiz)
      end
    end

    assert_equal author_reject_message, flash[:alert]
  end

  test "Player cannot create two responses" do
    quiz = FactoryBot.create(:playable_quiz)
    user = FactoryBot.create(:confirmed_user)
    sign_in user
    post :create, response_sheet: rs_params(quiz)

    assert_no_difference('ResponseSheet.count') do
      assert_no_difference('Response.count') do
        post :create, response_sheet: rs_params(quiz)
      end
    end

    assert_equal second_response_message, flash[:alert]
  end

  test "Cannot respond to non-playable quiz" do
    quiz = FactoryBot.create(:quiz, :closed_for_grading)
    user = FactoryBot.create(:confirmed_user)

    assert_no_difference('ResponseSheet.count') do
      assert_no_difference('Response.count') do
        sign_in user
        post :create, response_sheet: rs_params(quiz)
      end
    end

    assert_equal unplayable_quiz_message, flash[:alert]
  end

  private

  def rs_params (quiz)
    {
      quiz_id: quiz.id,
      responses_attributes: quiz.questions.map.with_index do |question, i|
        {
          question_id: question.id,
          response_text: "Response #{i}",
          money: i < quiz.money_question_count
          }
      end
      }
  end

  def successful_create_message
    'Successfully submitted!'
  end

  def author_reject_message
    "You cannot play your own quiz!"
  end

  def second_response_message
    "You have already submitted your answers for this quiz!"
  end

  def unplayable_quiz_message
    "This quiz is not open for participation."
  end

end
