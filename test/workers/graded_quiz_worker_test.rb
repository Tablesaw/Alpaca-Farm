require "test_helper"

class GradedQuizWorkerTest < ActiveSupport::TestCase

  test "perform should create a job" do
    Quiz.stub :find, FactoryBot.build(:quiz) do
      assert_difference("GradedQuizWorker.jobs.size", 1) do
        GradedQuizWorker.perform_async(1)
      end
    end
  end

  class GradedQuizWorkerTest::CreateNotices < GradedQuizWorkerTest
    def setup
      @worker = GradedQuizWorker.new
      @quiz = FactoryBot.create(:quiz_with_questions, :completed)
      @players = [
        FactoryBot.create(:player, :with_confirmed_user, graded_quiz: "immediately"),
        FactoryBot.create(:player, :with_confirmed_user, graded_quiz: "daily"),
        FactoryBot.create(:player, :with_confirmed_user)
        ]
      @players.each do |player|
        FactoryBot.create(:response_sheet, :scored,
                           player: player, quiz: @quiz)
      end
    end

    test "create_notices creates notifications for players who have opted in" do
      assert_difference "Notice::GradedQuiz.count", 2 do
        @worker.create_notices(@quiz)
      end
    end

    test "create_notices sends emails" do
      assert_difference "ActionMailer::Base.deliveries.count", 1 do
        Sidekiq::Extensions::DelayedMailer.clear
        @worker.create_notices(@quiz)
        Sidekiq::Extensions::DelayedMailer.drain
      end
    end

    test "create_notices does not send if a notice is invalid" do
      mock_notice = Minitest::Mock.new
      @players.size.times do
        mock_notice.expect :valid?, false
        # :send_notice is not mocked, will raise an error if called
      end

      Notice::GradedQuiz.stub :create, mock_notice do
        @worker.create_notices(@quiz)
      end

      pass
    end
  end
end
