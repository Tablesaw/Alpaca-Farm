# require "test_helper"

# class CompletedQuizWorkerTest < ActiveSupport::TestCase

#   test "perform should create a job" do
#     Quiz.stub :find, FactoryBot.build(:quiz) do
#       assert_difference("CompletedQuizWorker.jobs.size", 1) do
#         CompletedQuizWorker.perform_async(1)
#       end
#     end
#   end

#   class CompletedQuizWorkerTest::CreateNotices < CompletedQuizWorkerTest
#     def setup
#       @quiz = FactoryBot.create(:quiz_with_questions, :completed)
#       @players = FactoryBot.create_list(:player, 2, :with_user, :send_graded_quiz)
#       @players << FactoryBot.create(:player, :with_user)
#       @players.each do |player|
#         FactoryBot.create(:response_sheet, :scored,
#                            player: player, quiz: @quiz)
#       end
#     end

#     test "create_notices creates notifications for players who have opted in" do
#       assert_difference "Notice::CompletedQuiz.count", 2 do
#         CompletedQuizWorker.new.create_notices(@quiz)
#       end
#     end

#     test "create_notices sends emails" do
#       assert_difference "ActionMailer::Base.deliveries.count", 2 do
#         Sidekiq::Extensions::DelayedMailer.clear
#         CompletedQuizWorker.new.create_notices(@quiz)
#         Sidekiq::Extensions::DelayedMailer.drain
#       end
#     end

#     test "create_notices does not send if a notice is invalid" do
#       mock_notice = Minitest::Mock.new
#       @players.size.times do
#         mock_notice.expect :valid?, false
#         # :send_notice is not mocked, will raise an error if called
#       end

#       Notice::CompletedQuiz.stub :create, mock_notice do
#         CompletedQuizWorker.new.create_notices(@quiz)
#       end

#       pass
#     end
#   end
# end
