require "test_helper"

class DigestWorkerTest < ActiveSupport::TestCase

  test "perform should create a job" do
    Player.stub :find, FactoryBot.build(:player) do
      assert_difference("DigestWorker.jobs.size", 1) do
        DigestWorker.perform_async(1, "new_quiz")
      end
    end
  end

  class DigestWorkerTest::SendNotices < DigestWorkerTest
    def setup
      @worker = DigestWorker.new
    end

    test "send_notices sends a single digest when the notice frequency "\
         "is set to daily" do
      @player = FactoryBot.create(:player, :with_confirmed_user, new_quiz: "daily")
      FactoryBot.create(:new_quiz_notice, recipient: @player)

      assert_difference "ActionMailer::Base.deliveries.count", 1 do
        assert_difference "Notice::NewQuiz.unsent.count", -1 do
          Sidekiq::Extensions::DelayedMailer.clear
          @worker.send_notices(@player, "new_quiz")
          Sidekiq::Extensions::DelayedMailer.drain
        end
      end
    end

    test "send_notices does not send an email if notice frequency "\
         "is not set to a digest period" do
      @player = FactoryBot.create(:player, :with_confirmed_user, new_quiz: "immediately")
      FactoryBot.create(:new_quiz_notice, recipient: @player)
      assert_no_difference "ActionMailer::Base.deliveries.count" do
        Sidekiq::Extensions::DelayedMailer.clear
        @worker.send_notices(@player, "new_quiz")
        Sidekiq::Extensions::DelayedMailer.drain
      end
    end

    test "send_notices does not send an email if there are no notices" do
      @player = FactoryBot.create(:player, :with_confirmed_user, new_quiz: "daily")
      FactoryBot.create(:new_quiz_notice, :sent, recipient: @player)
      assert_no_difference "ActionMailer::Base.deliveries.count" do
        Sidekiq::Extensions::DelayedMailer.clear
        @worker.send_notices(@player, "new_quiz")
        Sidekiq::Extensions::DelayedMailer.drain
      end
    end

    test "send_notices sends a single digest when the notice type "\
         "is set to graded_quiz" do
      @player = FactoryBot.create(:player, :with_confirmed_user, graded_quiz: "daily")
      FactoryBot.create(:graded_quiz_notice, recipient: @player)

      assert_difference "ActionMailer::Base.deliveries.count", 1 do
        Sidekiq::Extensions::DelayedMailer.clear
        @worker.send_notices(@player, "graded_quiz")
        Sidekiq::Extensions::DelayedMailer.drain
      end
    end
  end
end
