require "test_helper"

class NewQuizWorkerTest < ActiveSupport::TestCase

  test "perform should create a job" do
    Quiz.stub :find, FactoryBot.build(:quiz) do
      assert_difference("NewQuizWorker.jobs.size", 1) do
        NewQuizWorker.perform_async(1)
      end
    end
  end

  class NewQuizWorkerTest::CreateNotices < NewQuizWorkerTest
    def setup
      @quiz = FactoryBot.create(:quiz, :opened, :uncloseable)
      @players = [
        FactoryBot.create(:player, :with_confirmed_user, new_quiz: "immediately"),
        FactoryBot.create(:player, :with_confirmed_user, new_quiz: "daily"),
        FactoryBot.create(:player, :with_confirmed_user)
        ]
    end

    test "create_notices creates notices for players who have opted in" do
      assert_difference "Notice::NewQuiz.count", 2 do
        NewQuizWorker.new.create_notices(@quiz)
      end
    end

    test "create_notices sends emails" do
      assert_difference "ActionMailer::Base.deliveries.count", 1 do
        Sidekiq::Extensions::DelayedMailer.clear
        NewQuizWorker.new.create_notices(@quiz)
        Sidekiq::Extensions::DelayedMailer.drain
      end
    end

    test "create_notices does not send if a notice is invalid" do
      mock_notice = Minitest::Mock.new
      @players.size.times do
        mock_notice.expect :valid?, false
        # :send_notice is not mocked, will raise an error if called
      end

      Notice::CompletedQuiz.stub :create, mock_notice do
        NewQuizWorker.new.create_notices(@quiz)
      end

      pass
    end
  end
end
