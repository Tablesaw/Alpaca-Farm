FactoryBot.define do
  factory :player do
    sequence(:name) { |n| "AlPaca#{n}" }
    opened_quizzes_count { 0 }

    trait :with_user do
      after :build do |player|
        player.user = FactoryBot.build(:user, player: player)
      end
    end

    trait :with_confirmed_user do
      after :build do |player|
        player.user = FactoryBot.build(:confirmed_user, player: player)
      end
    end

    # trait :send_new_quiz do
    #   new_quiz "immediately"
    # end

    # trait :send_graded_quiz do
    #   graded_quiz "immediately"
    # end
  end
end
