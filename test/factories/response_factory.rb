FactoryBot.define do
  factory :response do
    question
    response_text { "response text" }
    money { false }
  end

  trait :moneyed do
    money { true }
  end

  trait :correct do
    correct { true }
  end

  trait :incorrect do
    correct { false }
  end
end
