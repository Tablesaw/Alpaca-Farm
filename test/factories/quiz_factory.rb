FactoryBot.define do
  factory :raw_quiz, class: Quiz do
    association :author, factory: :player
    subject { "Test Quiz" }
    transient do
      questions { 5 }
      questions_with_responses { false }
      no_questions { false }
    end

    trait :with_default_values do
      base_score { 15 }
      money_question_count { 5 }
    end

    trait :opened do
      opened { true }
    end

    trait :closeable do
      approximate_grading_date_time {2.days.ago}
    end

    trait :uncloseable do
      approximate_grading_date_time {1.day.from_now}
    end

    trait :ready_for_grading do
      opened
      closeable
    end

    trait :closed_for_grading do
      opened
      closeable
      closed { true }
      closing_date_time { 1.day.ago }
    end

    trait :completed do
      closed_for_grading
      complete { true }
    end

    trait :no_questions do
      no_questions {true}
    end

    trait :with_questions do
      # all tests must have questions to be valid
    end

    trait :with_answered_questions do
      questions_with_responses {true}
      transient do
        responses {["correct", "incorrect", ""]}
      end

      # after :build do |quiz, eval|
      #   quiz.questions = build_list(:question_with_responses, 12,
      #     quiz: quiz,
      #     responses: eval.responses)
      # end
    end

    trait :fully_graded do
      with_questions
      closed_for_grading

      transient do
        players {3}
      end

      after :build do |quiz, eval|
        quiz.response_sheets = build_list(:response_sheet,
                                          eval.players,
                                          :with_graded_responses,
                                          quiz: quiz)
      end
    end

    after :build do |quiz, eval|
      unless eval.no_questions
        if eval.questions_with_responses
          quiz.questions = build_list(:question_with_responses, eval.questions,
            quiz: quiz,
            responses: eval.responses)
        else
          quiz.questions = build_list(:question, eval.questions, quiz: quiz)
        end
      end
    end

  end

  factory :quiz, parent: :raw_quiz, traits: [:with_default_values]
  factory :quiz_with_questions, parent: :quiz, traits: [:with_questions]
  factory :playable_quiz, parent: :quiz, traits: [:opened, :with_questions]

end
