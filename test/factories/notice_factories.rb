FactoryBot.define do
  factory :notice do

    trait :sent do
      sent_at { Time.now.utc - 1.hour }
    end

    trait :unsent do
      sent_at { nil }
    end

    trait :for_opened_quiz do
      association :about, factory: [:quiz, :opened, :uncloseable],
                          strategy: :build
    end

    trait :for_completed_quiz do
      association :about, factory: [:quiz, :completed],
                          strategy: :build
    end

    factory :new_quiz_notice, class: Notice::NewQuiz do
      for_opened_quiz
      immediately
      unsent

      trait :immediately do
        association :recipient,
                    factory: [:player, :with_confirmed_user],
                    strategy: :build,
                    new_quiz: "immediately"
      end

      trait :delayed do
        association :recipient,
                    factory: [:player, :with_confirmed_user],
                    strategy: :build,
                    new_quiz: "daily"
      end
    end

    factory :completed_quiz_notice, class: Notice::CompletedQuiz do
      for_completed_quiz
      immediately
      unsent

      trait :immediately do
        association :recipient,
                    factory: [:player, :with_confirmed_user],
                    strategy: :build
      end

      # trait :delayed do
      #   association :recipient,
      #               factory: [:player, :with_confirmed_user],
      #               strategy: :build
      #               completed_quiz: "daily"
      # end
    end

    factory :graded_quiz_notice, class: Notice::GradedQuiz do
      for_graded_response
      immediately
      unsent

      trait :for_graded_response do
        association :about, factory: [:response_sheet, :scored],
                            strategy: :build
      end

      trait :for_ungraded_response do
        association :about, factory: [:response_sheet, :with_responses],
                            strategy: :build
      end

      trait :immediately do
        association :recipient,
                    factory: [:player, :with_confirmed_user],
                    strategy: :build,
                    graded_quiz: "immediately"
      end

      trait :delayed do
        association :recipient,
                    factory: [:player, :with_confirmed_user],
                    strategy: :build,
                    graded_quiz: "daily"
      end
    end

  end
end
