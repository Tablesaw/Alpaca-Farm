FactoryBot.define do
  factory :response_sheet do
    association :player, strategy: :build
    association :quiz, factory: :quiz_with_questions, strategy: :build, money_question_count: 4

    trait :with_responses do
      after :build do |rs|
        moneys = rs.quiz.money_question_count
        rs.responses = []
        rs.quiz.questions.each do |question|
          rs.responses << build(:response, question: question, money: moneys > 0)
          moneys -= 1
        end
      end
    end

    trait :with_graded_responses do
      after :build do |rs|
        moneys = rs.quiz.money_question_count
        rs.responses = []
        rs.quiz.questions.each do |question|
          rs.responses << build(:response,
                                question: question,
                                money: moneys > 0,
                                correct: false)
          moneys -= 1
        end
      end
    end

    trait :scored do
      with_graded_responses
      rank {1}
      score {100}
    end
  end
end
