FactoryBot.define do
  factory :question do
    association :quiz, strategy: :build
    number { 1 }
    question_text { "The text of the question" }
    answer { "The text of the answer" }

    trait :edited_question do
      edited_text { "The edited text of the question" }
      question_edited_at { Time.now }
    end

    trait :edited_answer do
      edited_answer { "The edited text of the question" }
      answer_edited_at { Time.now }
    end

    # trait :with_responses do
    #   transient do
    #     number_of_responses 3
    #   end

    #   after :build do |question, evaluator|
    #     question.responses = build_list(:response,
    #                                     evaluator.number_of_responses,
    #                                     question: question)
    #   end

    # end
    trait :with_responses do
      transient do
        responses { ["correct", "incorrect", ""] }
      end

      after :build do |question, evaluator|
        question.responses = build_list(:response,
                                        evaluator.responses.size,
                                        question: question)
        question.responses.each_with_index do |response, i|
          response.response_text = evaluator.responses[i]
        end
      end

    end

    factory :question_with_responses, traits: [:with_responses]
  end
end
