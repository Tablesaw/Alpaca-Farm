FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "person#{n}@example.com" }
    password {"Passw0rd"}
    password_confirmation {"Passw0rd"}
    association :player, strategy: :build

    factory :confirmed_user do
      confirmed_at {Time.now}
    end
  end
end
