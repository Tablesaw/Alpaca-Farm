class AddExternalLinkToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :external_link, :string
  end
end
