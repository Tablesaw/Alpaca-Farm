class AddColumnsToResponseSheets < ActiveRecord::Migration
  def change
    add_column :response_sheets, :percentile_rank, :float
    add_column :response_sheets, :percent_of_champion, :float
  end
end
