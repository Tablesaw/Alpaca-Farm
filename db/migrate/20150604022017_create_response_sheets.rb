class CreateResponseSheets < ActiveRecord::Migration
  def change
    create_table :response_sheets do |t|
      t.references :player, index: true, foreign_key: true
      t.references :quiz, index: true, foreign_key: true
      t.integer :score
      t.integer :rank

      t.timestamps null: false
    end
  end
end
