class CreateQuizzes < ActiveRecord::Migration
  def change
    create_table :quizzes do |t|
      t.references :author, index: true, foreign_key:true
      t.string :subject
      t.integer :base_score
      t.integer :money_question_count
      t.integer :questions_count
      t.integer :response_sheets_count
      t.datetime :opening_date_time
      t.datetime :approximate_grading_date_time
      t.datetime :closing_date_time
      t.boolean :opened
      t.boolean :closed
      t.boolean :complete

      t.timestamps null: false
    end
  end
end
