class CreateNotices < ActiveRecord::Migration
  def change
    create_table :notices do |t|
      t.references :recipient, index: true
      t.references :about, index: true
      t.string :type
      t.datetime :sent_at

      t.timestamps null: false
    end
    add_foreign_key :notices, :players, column: :recipient_id
  end
end
