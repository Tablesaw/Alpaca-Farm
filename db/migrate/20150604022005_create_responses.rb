class CreateResponses < ActiveRecord::Migration
  def change
    create_table :responses do |t|
      t.references :question, index: true, foreign_key: true
      t.references :response_sheet, index: true, foreign_key: true
      t.string :response_text
      t.boolean :money
      t.boolean :correct

      t.timestamps null: false
    end
  end
end
