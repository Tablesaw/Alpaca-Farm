class AddResponsesCountToQuestions < ActiveRecord::Migration
  def self.up
    add_column :questions, :responses_count, :integer, null:false, default: 0
    Question.find_each {
      |question| Question.reset_counters(question.id, :responses)
      }
  end
  def self.down
    remove_column :questions, :responses_count
  end
end
