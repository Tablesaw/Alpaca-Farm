class AddAuthoredQuizzesCountToPlayer < ActiveRecord::Migration
  def change
    add_column :players, :authored_quizzes_count, :integer
  end
end
