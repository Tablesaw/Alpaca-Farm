class AddMaximumScoreToQuizzes < ActiveRecord::Migration
  def change
    add_column :quizzes, :maximum_score, :integer
  end
end
