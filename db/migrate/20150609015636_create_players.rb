class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.references :user, index: true, foreign_key: true
      t.string :name
      t.integer :response_sheets_count

      t.timestamps null: false
    end
  end
end
