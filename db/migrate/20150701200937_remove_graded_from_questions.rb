class RemoveGradedFromQuestions < ActiveRecord::Migration
  def change
    remove_column :questions, :graded, :boolean
  end
end
