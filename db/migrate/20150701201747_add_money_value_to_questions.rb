class AddMoneyValueToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :money_value, :integer
  end
end
