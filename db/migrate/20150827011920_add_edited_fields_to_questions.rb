class AddEditedFieldsToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :edited_text, :text
    add_column :questions, :question_edited_at, :datetime
    add_column :questions, :edited_answer, :string
    add_column :questions, :answer_edited_at, :datetime
  end
end
