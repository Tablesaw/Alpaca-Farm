class AddProfileInformationToPlayer < ActiveRecord::Migration
  def change
    add_column :players, :location, :string
    add_column :players, :motto, :string
  end
end
