class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.references :quiz, index: true, foreign_key: true
      t.integer :number
      t.text :question_text
      t.string :answer
      t.text :grading_notes
      t.boolean :graded

      t.timestamps null: false
    end
  end
end
