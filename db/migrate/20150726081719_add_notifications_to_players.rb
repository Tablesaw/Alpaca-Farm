class AddNotificationsToPlayers < ActiveRecord::Migration
  def change
    enable_extension 'hstore'
    add_column :players, :notifications, :hstore
    add_index :players, :notifications, using: :gist
  end
end
