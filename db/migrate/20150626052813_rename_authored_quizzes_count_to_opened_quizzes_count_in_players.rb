class RenameAuthoredQuizzesCountToOpenedQuizzesCountInPlayers < ActiveRecord::Migration
  def change
    rename_column :players, :authored_quizzes_count, :opened_quizzes_count
  end
end
