class AddReconfirmableToDevise < ActiveRecord::Migration
  def up
    add_column :users, :unconfirmed_email, :string # Only if using reconfirmable
    execute("UPDATE users SET confirmed_at = NOW()")
  end

  def down
    remove_columns :users, :unconfirmed_email # Only if using reconfirmable
  end
end
