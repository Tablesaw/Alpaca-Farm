# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151128092535) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"

  create_table "notices", force: :cascade do |t|
    t.integer  "recipient_id"
    t.integer  "about_id"
    t.string   "type"
    t.datetime "sent_at"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "notices", ["about_id"], name: "index_notices_on_about_id", using: :btree
  add_index "notices", ["recipient_id"], name: "index_notices_on_recipient_id", using: :btree

  create_table "players", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name"
    t.integer  "response_sheets_count"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.string   "location"
    t.string   "motto"
    t.integer  "opened_quizzes_count"
    t.string   "time_zone"
    t.hstore   "notifications"
  end

  add_index "players", ["notifications"], name: "index_players_on_notifications", using: :gist
  add_index "players", ["user_id"], name: "index_players_on_user_id", using: :btree

  create_table "questions", force: :cascade do |t|
    t.integer  "quiz_id"
    t.integer  "number"
    t.text     "question_text"
    t.string   "answer"
    t.text     "grading_notes"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "money_value"
    t.string   "external_link"
    t.integer  "responses_count",    default: 0, null: false
    t.text     "edited_text"
    t.datetime "question_edited_at"
    t.string   "edited_answer"
    t.datetime "answer_edited_at"
  end

  add_index "questions", ["quiz_id"], name: "index_questions_on_quiz_id", using: :btree

  create_table "quizzes", force: :cascade do |t|
    t.integer  "author_id"
    t.string   "subject"
    t.integer  "base_score"
    t.integer  "money_question_count"
    t.integer  "questions_count"
    t.integer  "response_sheets_count"
    t.datetime "opening_date_time"
    t.datetime "approximate_grading_date_time"
    t.datetime "closing_date_time"
    t.boolean  "opened"
    t.boolean  "closed"
    t.boolean  "complete"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.text     "description"
    t.integer  "maximum_score"
  end

  add_index "quizzes", ["author_id"], name: "index_quizzes_on_author_id", using: :btree

  create_table "response_sheets", force: :cascade do |t|
    t.integer  "player_id"
    t.integer  "quiz_id"
    t.integer  "score"
    t.integer  "rank"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.float    "percentile_rank"
    t.float    "percent_of_champion"
  end

  add_index "response_sheets", ["player_id"], name: "index_response_sheets_on_player_id", using: :btree
  add_index "response_sheets", ["quiz_id"], name: "index_response_sheets_on_quiz_id", using: :btree

  create_table "responses", force: :cascade do |t|
    t.integer  "question_id"
    t.integer  "response_sheet_id"
    t.string   "response_text"
    t.boolean  "money"
    t.boolean  "correct"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "responses", ["question_id"], name: "index_responses_on_question_id", using: :btree
  add_index "responses", ["response_sheet_id"], name: "index_responses_on_response_sheet_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "notices", "players", column: "recipient_id"
end
